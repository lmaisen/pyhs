# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 10:02:51 2023

@author: Lothar Maisenbacher/MPQ

Unit tests for `pyhs.statfunc`.
"""

import numpy as np

import pyhs.statfunc

# Single float as input, returning float and int
print('Single float as input, returning float and int')
a = 1.
sa = 1.
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa)
print(wavg)
assert isinstance(wavg['Mean'], float)
assert isinstance(wavg['NPoints'], int)
assert wavg['Mean'] == 1.
assert wavg['Sigma'] == 1.
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9,)

# 1-D length-N array as input, no uncertainties or weights, returning float and int
print('\n1-D length-N array as input, no uncertainties or weights, returning float and int')
a = np.arange(1, 11)
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a)
print(wavg)
assert isinstance(wavg['Mean'], float)
assert isinstance(wavg['NPoints'], int)
assert wavg['Mean'] == np.mean(a)
assert np.isnan(wavg['Sigma'])
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9,)

# 1-D length-N array as input, uncertainties only, returning float and int
print('\n1-D length-N array as input, uncertainties only, returning float and int')
a = np.arange(1, 11)
sa = np.ones(len(a))
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa)
print(wavg)
assert isinstance(wavg['Mean'], float)
assert isinstance(wavg['NPoints'], int)
assert wavg['Mean'] == np.mean(a)
assert wavg['Sigma'] == np.sqrt(1/np.sum(1/sa**2))
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9,)

# 1-D length-N array as input, uncertainties only, returning float and int
print('\n1-D length-N array as input, uncertainties only, returning float and int')
a = np.arange(1, 11)
sa = np.ones(len(a))
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa)
print(wavg)
assert isinstance(wavg['Mean'], float)
assert isinstance(wavg['NPoints'], int)
assert wavg['Mean'] == np.mean(a)
assert wavg['Sigma'] == np.sqrt(1/np.sum(1/sa**2))
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9,)

# 1-D length-N array as input, uncertainties + weights, returning float and int
print('\n1-D length-N array as input, uncertainties + weights, returning float and int')
a = np.hstack((np.ones(5), 2*np.ones(5)))
sa = np.ones(len(a))
weights = np.hstack((2*np.ones(5), np.ones(5)))
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, weights=weights)
print(wavg)
assert isinstance(wavg['Mean'], float)
assert isinstance(wavg['NPoints'], int)
assert wavg['Mean'] == np.sum(a*weights)/np.sum(weights)
assert wavg['Sigma'] == np.sqrt(np.sum(weights**2*sa**2/np.sum(weights)**2))
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, weights=weights, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9,)

# 2-D N x 1 array as input, returning zero-length array, float, and int
print('\n2-D N x 1 array as input, returning zero-length array, float, and int')
a = np.arange(1, 11)
sa = np.ones(len(a))
a = a.reshape((len(a), 1))
sa = sa.reshape((len(a), 1))
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa)
print(wavg)
assert isinstance(wavg['Mean'], np.ndarray)
assert isinstance(wavg['NPoints'], np.ndarray)
assert wavg['Mean'].shape == ()
assert wavg['Mean'] == np.mean(a)
assert wavg['Sigma'] == np.sqrt(1/np.sum(1/sa**2))
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9,)

# 2-D N x 1 array as input, without squeeze, returning length-1 arrays
print('\n2-D N x 1 array as input, without squeeze, returning length-1 arrays')
a = np.arange(1, 11)
sa = np.ones(len(a))
a = a.reshape((len(a), 1))
sa = sa.reshape((len(a), 1))
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, squeeze=False)
print(wavg)
assert isinstance(wavg['Mean'], np.ndarray)
assert isinstance(wavg['NPoints'], np.ndarray)
assert wavg['Mean'].shape == (1,)
assert wavg['Mean'] == np.mean(a)
assert wavg['Sigma'] == np.sqrt(1/np.sum(1/sa**2))
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9,)

# 2-D N x 2 array as input, returning 1-D length-2 array
print('\n2-D N x 2 array as input, returning 1-D length-2 array')
a1 = np.arange(1, 10)
a2 = np.arange(11, 20)
sa1 = np.ones(len(a1))
sa2 = 2*np.ones(len(a1))
a = np.vstack((a1, a2)).transpose()
sa = np.vstack((sa1, sa2)).transpose()
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa)
print(wavg)
assert isinstance(wavg['Mean'], np.ndarray)
assert wavg['Mean'].shape == (2,)
assert (wavg['Mean'] == np.array([5., 15.])).all()
assert (wavg['Sigma'] == np.array([np.sqrt(1/np.sum(1/sa1**2)), np.sqrt(1/np.sum(1/sa2**2))])).all()
assert isinstance(wavg['NPoints'], np.ndarray)
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9, 2)

# 2-D N x 2 array as input, no uncertainties or weights, returning 1-D length-2 array
print('\n2-D N x 2 array as input, no uncertainties or weights, returning 1-D length-2 array')
a1 = np.arange(1, 10)
a2 = np.arange(11, 20)
a = np.vstack((a1, a2)).transpose()
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a)
print(wavg)
assert isinstance(wavg['Mean'], np.ndarray)
assert wavg['Mean'].shape == (2,)
assert (wavg['Mean'] == np.array([5., 15.])).all()
assert np.isnan(wavg['Sigma']).all()
assert isinstance(wavg['NPoints'], np.ndarray)
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, return_dict=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (9, 2)

# 2-D N x 2 array as input, reduced output, returning 1-D length-2 array
print('\n2-D N x 2 array as input, reduced output, returning 1-D length-2 array')
a1 = np.arange(1, 10)
a2 = np.arange(11, 20)
sa1 = np.ones(len(a1))
sa2 = 2*np.ones(len(a1))
a = np.vstack((a1, a2)).transpose()
sa = np.vstack((sa1, sa2)).transpose()
# Return dict as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, full_output=False)
print(wavg)
assert isinstance(wavg['Mean'], np.ndarray)
assert wavg['Mean'].shape == (2,)
assert (wavg['Mean'] == np.array([5., 15.])).all()
assert (wavg['Sigma'] == np.array([np.sqrt(1/np.sum(1/sa1**2)), np.sqrt(1/np.sum(1/sa2**2))])).all()
# Return array as output
wavg = pyhs.statfunc.avg_uncorr_meas(a=a, sa=sa, return_dict=False, full_output=False)
print(wavg)
assert isinstance(wavg, np.ndarray)
assert wavg.shape == (4, 2)
