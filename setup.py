import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='pyhs',
    version='0.1',
    author='Lothar Maisenbacher',
    author_email='lothar.maisenbacher@mpq.mpg.de',
    description='Helper functions and methods of pythonic hydrogen (pyh).',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.mpcdf.mpg.de/lmaisen/pyhs',
    packages=['pyhs', 'pyhs.devices'],
    package_data={'pyhs': ['examples/*']},
    install_requires=[
        'numpy',
        'scipy',
        'pandas',
        'numba',
        'xlsxwriter',
        'astropy',
        'pyvisa',
        ],
)
