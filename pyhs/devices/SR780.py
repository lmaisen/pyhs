# -*- coding: utf-8 -*-
"""
Created on Sat Mar  3 16:46:03 2018

@author: Lothar Maisenbacher/MPQ

Definitions and functions for communication with SRS SR780 FFT network signal analyzer.
"""

import numpy as np

import pyhs.devices.genVISAdev

## Device functions
class SR780(pyhs.devices.genVISAdev.GenVISAdev):

    ## Define device constants

    # Units
    units_code = {
            0: 'Vpk',
            1: 'Vrms',
            2: 'Vpk2',
            3: 'Vrms2',
            4: 'dBVpk',
            5: 'dBVrms',
            6: 'dBm',
            7: 'dBspl',
            # Phase view only
            8: 'deg',
            9: 'radian',
            # Transfer function only
            10: 'Units',
            11: 'dB',
            }

    units_log = {
            'Vpk': False,
            'Vrms': False,
            'Vpk2': False,
            'Vrms2': False,
            'dbVpk': True,
            'dBVrms': True,
            'dBm': True,
            'dBspl': True,
            'deg': False,
            'radian': False,
            'Units': False,
            'dB': True,
            }

    units_label = {
            'Vpk': 'V$_\mathrm{pk}$',
            'Vrms': 'V$_\mathrm{rms}$',
            'Vp2': 'V$_\mathrm{pk}^2$',
            'Vrms2': 'V$_\mathrm{rms}^2$',
            'dbVpk': 'dBV$_\mathrm{pk}$',
            'dBVrms': 'dBV$_\mathrm{rms}$',
            'dBm': 'dBm',
            'dBspl': 'dBspl',
            'deg': '$^\circ$',
            'radian': 'rad',
            'Units': 'Units',
            'dB': 'dB',
            }

    # Measurements
    meas_code = {
            0: 'FFT1',
            1: 'FFT2',
            30: 'Spectrum1',
            31: 'Spectrum2',
            32: 'CrossSpectrum',
            33: 'TransferFunction',
            }

    # Views
    view_code = {
            0: 'Log Magnitude',
            1: 'Linear Magnitude',
            2: 'Magnitude Squared',
            3: 'Real Part',
            4: 'Imaginary Part',
            5: 'Phase',
            6: 'Unwrapped Phase',
            7: 'Nyquist',
            8: 'Nichols',
            }

    view_label = {value: value for key, value in view_code.items()}
    view_label['Log Magnitude'] = 'Magnitude'
    view_label['Linear Magnitude'] = 'Magnitude'

    ## Define instrument parameters
    # Communication timeout (in ms)
    INST_TIMEOUT = 4000

    ## Define data parameters
    # String
    PARAMS_ELEMS_STR = ['Meas','View','MeasType','Unit']
    # Int
    PARAMS_ELEMS_INT = ['NLines','NPoints','NAvg']
    # Float
    PARAMS_ELEMS_FLOAT = ['FreqSpan','FreqStart','FreqStop']
    # Bool
    PARAMS_ELEMS_BOOL = ['Avg','PSDunits']
    # List
    PARAMS_ELEMS_LIST = []
    # Default values for specific params
    PARAMS_DEFAULT_SPECIFIC = {}

    ## Define default values of variables
    DEFAULT_XNAME = 'Frequency'
    DEFAULT_XUNIT = 'Hz'
    DEFAULT_YNAME = 'Magnitude'
    DEFAULT_YUNIT = units_code[4]
    DEFAULT_CHNAME = 'Ch'
    DEFAULT_CHUNIT = DEFAULT_YUNIT

    # Open instrument connection
    def openConn(self, address, timeout=INST_TIMEOUT):

        self._openConn(address, timeout=timeout)
        self.instH.write_termination = '\n'
        self.instH.read_termination = '\n'
        # Set device output to GPIB (by default, the SR780 answers on the RS232 port)
        self.instH.write('OUTX 0')
        self.readID()

    # Read current parameters from device
    def getParams(self, display=0):

        # Query parameters
        params = {}
        mgrp = int(self.instH.query('MGRP? {:d}'.format(display)))
        meas = int(self.instH.query('MEAS? {:d}'.format(display)))
        params['Meas'] = self.meas_code[meas]
        view = int(self.instH.query('VIEW? {:d}'.format(display)))
        params['View'] = self.view_code[view]
        if mgrp == 0:
            # FFT measurement
            params['MeasType'] = 'FFT'
            # Number of FFT points
            FFTnPoints = int(self.instH.query('FLIN? {:d}'.format(display)))
            params['NLines'] = (2**FFTnPoints)*100
            params['NPoints'] = params['NLines'] + 1
            # FFT span, in Hz
            params['FreqSpan'] = float(self.instH.query('FSPN? {:d}'.format(display)))
            # FFT start freq, in Hz
            params['FreqStart'] = float(self.instH.query('FSTR? {:d}'.format(display)))
            # FFT stop freq, in Hz
            params['FreqStop'] = float(self.instH.query('FEND? {:d}'.format(display)))
            # FFT averaging status
            params['Avg'] = bool(int(self.instH.query('FAVG? {:d}'.format(display))))
            # FFT averaging samples
            params['NAvg'] = int(self.instH.query('FAVN? {:d}'.format(display)))
            freq = np.append(
                np.arange(
                    params['FreqStart'],params['FreqStop'],params['FreqSpan']/params['NLines']),
                params['FreqStop'])
        elif mgrp == 2:
            # Swept sine measurement
            params['MeasType'] = 'Swept sine'
            # Number of FFT points
            FFTnPoints = int(self.instH.query('SNPS? {:d}'.format(display)))
            params['NLines'] = -1
            params['NPoints'] = FFTnPoints
            # FFT start freq, in Hz
            params['FreqStart'] = float(self.instH.query('SSTR? {:d}'.format(display)))
            # FFT stop freq, in Hz
            params['FreqStop'] = float(self.instH.query('SSTP? {:d}'.format(display)))
            params['FreqSpan'] = params['FreqStop']-params['FreqStart']
            params['Avg'] = False
            params['NAvg'] = 1
            freq = np.linspace(params['FreqStart'],params['FreqStop'],params['NPoints'])
        unitCode = int(self.instH.query('UNIT? {:d}'.format(display)))
        params['Unit'] = self.units_code[unitCode]
        params['PSDunits'] = bool(int(self.instH.query('PSDU? {:d}'.format(display))))
        params = {**self.defaultParams, **params}

        return params, freq

#    # Read data and parameters from file
#    def readTFfile(self, filename):
#
#        # Read params from header
#        params = self.readParams(filename)
#        params = self.castParams(params)
#        params = {**self.defaultParams, **params}
#        # Read data
#        rawData = self.readRawFile(filename)
#        freq = rawData[:,0]
#        TFdata = rawData[:,1:].transpose()
#
#        return params, freq, TFdata
