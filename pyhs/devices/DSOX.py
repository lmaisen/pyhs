# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Connects to and reads data from Agilent/Keysight DSOX oscilloscopes
"""

import numpy as np
import time

import pyhs.devices.genVISAdev

class DSOX(pyhs.devices.genVISAdev.GenVISAdev):

    ## Define instrument parameters
    INST_TIMEOUT = 4000 # Communication timeout in ms

    ## Define data parameters
    # String
    PARAMS_ELEMS_STR = ['Mode']
    # Int
    PARAMS_ELEMS_INT = ['Samples']
    # Float
    PARAMS_ELEMS_FLOAT = []
    # Bool
    PARAMS_ELEMS_BOOL = []
    # List
    PARAMS_ELEMS_LIST = []
    # Default values for specific params
    PARAMS_DEFAULT_SPECIFIC = {}

    ## Define default values of variables
    DEFAULT_XNAME = 'Time'
    DEFAULT_XUNIT = 's'
    DEFAULT_YNAME = 'Voltage'
    DEFAULT_YUNIT = 'V'
    DEFAULT_CHNAME = 'Ch'
    DEFAULT_CHUNIT = DEFAULT_YUNIT

    def confDataTransfer(self):

        # Send signed data
        query = ':WAVeform:UNSigned OFF'
        self.instH.write(query)

        # Set byte order to little-endian
        query = ':WAVeform:BYTeorder LSBFirst'
        self.instH.write(query)

        # Set data format to WORD (16 bit)
        query = ':WAVeform:FORMat WORD'
        self.instH.write(query)

        # Send raw data
        query = ':WAVeform:POINts:MODE RAW'
        self.instH.write(query)

    # Get nPoints points for each channel
    def setNPoints(self, nPoints):

        query = ':WAVeform:POINts {:d}'.format(int(nPoints))
        self.instH.write(query)

    # Get maximum number of points available for each channel
    def setMaxNPoints(self):

        query = ':WAVeform:POINts MAXimum'
        self.instH.write(query)

    def readTimebase(self):

        query = ':WAVEFORM:PREAMBLE?'
        preamble = self.instH.query(query)
        preamble = [float(elem) for elem in preamble.rstrip().split(',')]
        modeID = int(preamble[1])
        modes = {0: 'Normal', 1: 'Peak detect', 2: 'Average', 3: 'High res'}
        if modeID in modes.keys():
            mode = modes[modeID]
        else:
            mode = 'Unknown'
        nPoints = int(preamble[2])
        xincrement = preamble[4]
        xorigin = preamble[5]
        timebase = np.arange(nPoints)*xincrement+xorigin
        self.params['Mode'] = mode
        self.params['Samples'] = len(timebase)
        return timebase, self.params

    def readChannel(self, chID):

        # Set channel
        query = ':WAVeform:SOURce CHAN{:d}'.format(chID)
        response = self.instH.write(query)

        query = ':WAVeform:POINts?'
        nPoints = self.instH.query(query)
        nPoints = int(nPoints.rstrip())
        print('Reading {:d} points from channel {:d}'.format(nPoints,chID))

        query = ':WAVeform:YINCrement?'
        yincrement = self.instH.query(query)
        yincrement = float(yincrement.rstrip())

        query = ':WAVeform:YORIgin?'
        yorigin = self.instH.query(query)
        yorigin = float(yorigin.rstrip())

        query = ':WAVeform:YREFerence?'
        yreference = self.instH.query(query)

        query = ':WAVeform:DATA?'
        data = self.instH.query_binary_values(query, datatype='h', container=np.array)

        data = data*yincrement+yorigin
        return data

    def readChannels(self, chIIDs):

        self.xData, mode = self.readTimebase()
        self.params['Time'] = time.strftime("%Y-%m-%d %H-%M-%S")
        if self.params['Mode']=='Peak detect':
            nValues = 2 * len(self.xData)
            self.xData = np.repeat(self.xData, 2)
        else:
            nValues = len(self.xData)
        self.data = np.zeros((len(chIIDs),nValues))

        self.params['NChannels'] = len(chIIDs)
        for i, chIID in enumerate(chIIDs):
            self.data[i] = self.readChannel(chIID)
        self.params['Columns'] =  [self.DEFAULT_XNAME] + [self.DEFAULT_CHNAME + '{:d}'.format(chIID) for chIID in chIIDs]
        self.params['ColumnUnits'] =  [self.DEFAULT_XUNIT] + [self.DEFAULT_CHUNIT for chIID in chIIDs]
        self.chIIDs = chIIDs
        self._setColumnNames()

        self.dataPresent = True
        return self.xData, self.data, self.params
