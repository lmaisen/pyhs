# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Definitions and functions for communication with a generic VISA device
"""

import numpy as np
import pyvisa as visa
import gzip
import os
import io
import csv
import re

## Device functions
class GenVISAdev():

    ## Define instrument parameters
    # Communication timeout (in ms)
    INST_TIMEOUT = 4000
    # Termination character for instrument read, default is None
    READ_TERMINATION = None
    # Termination character for instrument write, default is '\r\n\'
    WRITE_TERMINATION = '\r\n'

    ## Define data parameters
    # String
    GEN_PARAMS_ELEMS_STR = ['Manufacturer','Model','Serial number','Time','Comment']
    PARAMS_ELEMS_STR = []
    PARAMS_DEFAULT_STR = 'Unknown'
    # Int
    GEN_PARAMS_ELEMS_INT = ['NChannels']
    PARAMS_ELEMS_INT = []
    PARAMS_DEFAULT_INT = -1
    # Float
    GEN_PARAMS_ELEMS_FLOAT = []
    PARAMS_ELEMS_FLOAT = []
    PARAMS_DEFAULT_FLOAT = np.nan
    # Bool
    GEN_PARAMS_ELEMS_BOOL = []
    PARAMS_ELEMS_BOOL = []
    PARAMS_DEFAULT_BOOL = False
    # List
    GEN_PARAMS_ELEMS_LIST = ['Columns','ColumnNames','ColumnUnits']
    PARAMS_ELEMS_LIST = []
    PARAMS_DEFAULT_LIST = []
    # Default values for specific params
    PARAMS_DEFAULT_SPECIFIC = {}

    ## Define default values of variables
    DEFAULT_XNAME = 'x'
    DEFAULT_XUNIT = ''
    DEFAULT_YNAME = 'y'
    DEFAULT_YUNIT = ''
    DEFAULT_CHNAME = 'Ch'
    DEFAULT_CHUNIT = ''

    ## Init variables
    params = {}
    chIIDs = []
    xName = ''
    xUnit = ''
    chNames = {}
    chUnits = {}
    dataPresent = False

    # Init class
    def __init__(self):

        self.instH = None
        # Set default values for parameters
        self._setDefaultParams()

    # Open instrument connection
    def _openConn(self, address, timeout=INST_TIMEOUT):
        VISArm = visa.ResourceManager()
        instH = VISArm.open_resource(address)
        instH.timeout = timeout
        instH.read_termination = self.READ_TERMINATION
        instH.write_termination = self.WRITE_TERMINATION
        self.instH = instH

    def openConn(self, address, **kwargs):

        self._openConn(address, **kwargs)
        self._initDevice()
        self.readID()

    # Close instrument connection
    def _closeConn(self):

        self.instH.close()

    def closeConn(self):

        self._closeConn()

    # Read instrument ID
    def readID(self):

        query = '*IDN?'
        instID = self.instH.query(query)
        instID = instID.rstrip()
        hwinfo = instID.split(',')
        self.params['Manufacturer'] = hwinfo[0]
        self.params['Model'] = hwinfo[1]
        self.params['Serial number'] = hwinfo[2]
        return self.params

    # Init device
    def _initDevice(self):

        None

    # Set default values for parameters
    def _setDefaultParams(self):

        self.xName = self.DEFAULT_XNAME
        self.xUnit = self.DEFAULT_XUNIT
        self.yName = self.DEFAULT_YNAME
        self.yUnit = self.DEFAULT_YUNIT
        self.PARAMS_ELEMS_STR = list(set(self.GEN_PARAMS_ELEMS_STR + self.PARAMS_ELEMS_STR))
        self.PARAMS_ELEMS_INT = list(set(self.GEN_PARAMS_ELEMS_INT + self.PARAMS_ELEMS_INT))
        self.PARAMS_ELEMS_FLOAT = list(set(self.GEN_PARAMS_ELEMS_FLOAT + self.PARAMS_ELEMS_FLOAT))
        self.PARAMS_ELEMS_BOOL = list(set(self.GEN_PARAMS_ELEMS_BOOL + self.PARAMS_ELEMS_BOOL))
        self.PARAMS_ELEMS_LIST = list(set(self.GEN_PARAMS_ELEMS_LIST + self.PARAMS_ELEMS_LIST))
        strParams = {elem: self.PARAMS_DEFAULT_STR for elem in self.PARAMS_ELEMS_STR}
        intParams = {elem: self.PARAMS_DEFAULT_INT for elem in self.PARAMS_ELEMS_INT}
        floatParams = {elem: self.PARAMS_DEFAULT_FLOAT for elem in self.PARAMS_ELEMS_FLOAT}
        boolParams = {elem: self.PARAMS_DEFAULT_BOOL for elem in self.PARAMS_ELEMS_BOOL}
        listParams = {elem: self.PARAMS_DEFAULT_LIST for elem in self.PARAMS_ELEMS_LIST}
        params = {**strParams,**intParams,**floatParams,**boolParams, **listParams}
        self.defaultParams = {**params, **self.PARAMS_DEFAULT_SPECIFIC}

    # Cast params read from file into defined data types
    def _castParams(self, params):

        for elem in self.PARAMS_ELEMS_STR:
            if elem in params:
                params[elem] = str(params[elem])
        for elem in self.PARAMS_ELEMS_INT:
            if elem in params:
                params[elem] = int(params[elem])
        for elem in self.PARAMS_ELEMS_FLOAT:
            if elem in params:
                params[elem] = float(params[elem])
        for elem in self.PARAMS_ELEMS_BOOL:
            if elem in params:
                params[elem] = bool(params[elem])
        for elem in self.PARAMS_ELEMS_LIST:
            if elem in params:
                params[elem] = list(params[elem])
        return params

    # Read header from main file or from header file and extract data parameters
    def _readHeader(self, filename):

        filebase, file_extension = os.path.splitext(filename)
        openFct = open
        filename_header = filename
        if file_extension == '.gz':
            openFct = lambda filename: gzip.open(filename,'rt')
        elif file_extension=='.npy':
            filename_header = filebase + '_header.dat'
        with openFct(filename_header) as f:
            params = {}
            for line in f:
                if line[0]!='#':
                    break
                line = line.rstrip()
                t = line.find(':')
                key = line[2:t]
                params[key] = self._unescapeCSVstring(key, line[t+2:])
        return params

    # Read data file with header
    def _readFile(self, filename):

        filebase, file_extension = os.path.splitext(filename)
        if file_extension == '.npy':
            # Read params from header
            params = self._readHeader(filename)
            rawData = np.load(filename)
        elif file_extension == '.npz':
            rawDataContainer = np.load(filename, allow_pickle=True)
            rawData = rawDataContainer['rawData']
            params = rawDataContainer['params'][()]
            rawDataContainer.close()
        else:
            # Read params from header
            params = self._readHeader(filename)
            rawData = np.loadtxt(filename, delimiter=',')
        params = self._castParams(params)
        params = {**self.defaultParams, **params}
        self._readColumns(params)
        return rawData, params

    # Read data and parameters from file
    def readFile(self, filename):

        # Read data
        rawData, params = self._readFile(filename)
        self.params = params
        self.data = rawData[:,1:].transpose()
        self.xData = rawData[:,0]

        self.dataPresent = True
        return self.xData, self.data, self.params

    # Save data to file, with the format depending on file extension
    # For .npy extension, the data itself is written in npy format, while the header is written as plain text
    # For .gz extension, the data and the header are written as gzip-compressed plain text
    # For any other extension, the data and the header are written as uncompressed plain text
    def _writeFile(self, filename, rawData, params):

#        for key, value in self.params.items():
#            print(str(value))
#            print(self._escapeCSVstring(str(value)))
        headerDict = [key + ': ' + self._escapeCSVstring(key, value).rstrip() for key, value in self.params.items()]
        header = '\n'.join(headerDict)
        filebase, file_extension = os.path.splitext(filename)
        if file_extension=='.npy':
            np.savetxt(filebase + '_header.dat', [], delimiter=',', header=header)
            np.save(filename, rawData)
        elif file_extension=='.npz':
            np.savez_compressed(filename, rawData=rawData, params=params)
        else:
            np.savetxt(filename, rawData, delimiter=',', header=header)

    # Write data and parameters to file
    def writeFile(self, filename=''):

        if self.dataPresent:
            self._setColumnNames()
            # Save data to file
            rawData = np.transpose(np.vstack((self.xData,self.data)))
            self._writeFile(filename, rawData, self.params)
        else:
            filename = ''
        return filename

    def _escapeCSVstring(self, key, unescStr):

        output = io.StringIO()
        writer = csv.writer(output)
        if key in self.PARAMS_ELEMS_LIST:
            writer.writerow(unescStr)
        else:
            writer.writerow([unescStr])
        return output.getvalue()

    def _unescapeCSVstring(self, key, escStr):

        escStrIO = io.StringIO(escStr)
        reader = csv.reader(escStrIO)
        unescStr = ''
        for row in reader:
            if key in self.PARAMS_ELEMS_LIST:
                unescStr = [elem.lstrip() for elem in row]
            else:
                unescStr = row[0]
            break
        return unescStr

    def setComment(self, comment):

        self.params['Comment'] = str(comment)

    def setChannelNames(self, chNames):

        self.chNames = dict(chNames)

    def _setColumnNames(self):

        self.params['ColumnNames'] =  ['Time'] + [self.chNames[chIID] if chIID in self.chNames else 'Ch{:d}'.format(chIID) for chIID in self.chIIDs]

    # Parse column string from parameters to extract channel IIDs and names
    def _readColumns(self, params):


        chIIDs = [-1] * (len(params['Columns'])-1)
        chNames = {}
        chUnits = {}
        if len(params['ColumnNames']) > 0:
            self.xName = params['ColumnNames'][0]
        else:
            self.xName = self.DEFAULT_XNAME
        if len(params['ColumnUnits']) > 0:
            self.xUnit = params['ColumnUnits'][0]
        else:
            self.xUnit = self.DEFAULT_XUNIT
        for i,column in enumerate(params['Columns'][1:]):
            pm = re.match(self.DEFAULT_CHNAME + '[0-9]+', column)
            if pm:
                chIIDs[i] = int(pm[0][2:])
            else:
                chIIDs[i] = i+1
            if len(params['ColumnNames']) > i+1:
                chNames[chIIDs[i]] = params['ColumnNames'][i+1]
            else:
                chNames[chIIDs[i]] = self.DEFAULT_CHNAME + '{:d}'.format(chIIDs[i])
            if len(params['ColumnUnits']) > i+1:
                chUnits[chIIDs[i]] = params['ColumnUnits'][i+1]
            else:
                chUnits[chIIDs[i]] = self.DEFAULT_CHUNIT
        self.chIIDs = chIIDs
        self.chNames = chNames
        self.chUnits = chUnits
        return params
