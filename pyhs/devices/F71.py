# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Connects to and reads data from LakeShore F71 teslameter
"""

import pyhs.devices.genVISAdev

class F71(pyhs.devices.genVISAdev.GenVISAdev):

    ## Define instrument parameters
    # Communication timeout (in ms)
    INST_TIMEOUT = 4000
    # Termination character for instrument read
    READ_TERMINATION = '\n'
    # Termination character for instrument write
    WRITE_TERMINATION = '\r\n'

    ## Define data parameters
    # String
    PARAMS_ELEMS_STR = []
    # Int
    PARAMS_ELEMS_INT = ['NAvg']
    # Float
    PARAMS_ELEMS_FLOAT = []
    # Bool
    PARAMS_ELEMS_BOOL = []
    # List
    PARAMS_ELEMS_LIST = []
    # Default values for specific params
    PARAMS_DEFAULT_SPECIFIC = {}

    ## Define default values of variables
    DEFAULT_XNAME = 'Time'
    DEFAULT_XUNIT = 's'
    DEFAULT_YNAME = 'Field'
    DEFAULT_YUNIT = 'mG'
    DEFAULT_CHNAME = 'Ch'
    DEFAULT_CHUNIT = DEFAULT_YUNIT

    ## Instrument constants
    F71_SAMPLE_TIME = 10e-3

    # Init device
    def _initDevice(self):

        # Configure device
        self.confInst()

        # Get current number of averages
        self.getAveraging()

    def confDataTransfer(self):

        None

    def confInst(self):

        # Set unit to Gauss
        query = 'UNIT GAUSS'
        self.instH.write(query)

    def getAveraging(self):

        navg = self.instH.query('SENSe:AVERage:COUNt?')
        self.params['NAvg'] = int(navg.rstrip())

        return navg

    def setAveraging(self, navg_req):

        # Set integer number of 10 ms samples to average
        query = 'SENSe:AVERage:COUNt {:d}'.format(navg_req)
        self.instH.write(query)

        return self.getAveraging()

    # Get time interval needed for device to acquire the set amount of averages
    def getAvgTime(self):

        return self.params['NAvg'] * self.F71_SAMPLE_TIME

    def readField(self):

        # Read magnetic field in x, y, z and magnitude
        resp = self.instH.query('FETCh:FIELd:DC? ALL')
        names = ['Magn', 'X', 'Y', 'Z']
        self.data = {name: float(value)*1e3 for name, value in zip(names , resp.rstrip().split(','))}

        return self.data
