# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Definition of fit functions and associated functions.
"""

import numpy as np
import scipy.special

## Linear
def Linear(x, s, y0):
    """
    Linear function in `x` with slope `s` and offset `y0` at `x = 0`.
    """
    return s*x + y0

def Df_Linear(x, *p):
    """
    Partial derivatives for function Linear.
    """
    Dfun = np.zeros((2, len(x)))
    Dfun[0, :] = x
    Dfun[1, :] = 1
    Dfun = Dfun.transpose()
    return Dfun

def Sigma_Linear(x, p, pcov):
    """
    Uncertainty of linear function in `x`.
    """
    return np.sqrt(
        pcov[0, 0]*x**2 + pcov[1, 1]  + 2*pcov[0, 1]*x
        )

def Linear_x0(p, pcov):
    """
    Get value and uncertainty of `x0` (value of x-axis crossing)
    from results of fit of function `Linear`.
    """
    x0 = -p[1]/p[0]
    x0_sigma = np.sqrt(
        pcov[0, 0]*(p[1]/p[0]**2)**2 + \
        pcov[1, 1]*(1/p[0])**2 + \
        -2*pcov[0, 1]*(p[1]/p[0]**3)
        )
    return x0, x0_sigma

def Linear_crossing_point(
        y0, s0, dy0, ds0, dys0,
        y1, s1, dy1, ds1, dys1
        ):
    """
    Find crossing point and associated uncertainty of two
    lines parametrized as function `Linear`.
    """
    xc = (-y0 + y1)/(s0 - s1)
    xc_sigma = np.sqrt(
        (
            dy0**2*(s0 - s1)**2
            + dy1**2*(s0 - s1)**2
            + (y0 - y1)*(
                -(dys0 + dys1)*(s0 - s1)
                + (ds0**2 + ds1**2)*(y0 - y1)
            )
        ) / (s0 - s1)**4)
    yc = (y0 - (s0*y1)/s1)/(1 - s0/s1)
    yc_sigma = np.sqrt(
        (
            dy1**2*s0**2*(s0 - s1)**2
            + dy0**2*(s0 - s1)**2*s1**2
            + (y0 - y1)*(
                dys1*s0*(-s0 + s1)
                + dys0*s1*(-s0 + s1)
                + (ds1**2*s0**2 + ds0**2*s1**2)*(y0 - y1)
            )
        )/(s0 - s1)**4)

    return [xc, yc], [xc_sigma, yc_sigma]

## Parabola
def Parabola(x, b, x0, y0):
    """
    Parabola in `x` with quadratic coefficient `b`, centered at `x0` and offset `y0` at `x = x0`.
    """
    return b*(x-x0)**2 + y0

def Df_Parabola(x, *p):
    """
    Partial derivatives for function `Parabola`.
    """
    Dfun = np.zeros((3, len(x)))
    Dfun[0, :] = (x-p[1])**2
    Dfun[1, :] = -2*p[0]*(x-p[1])
    Dfun[2, :] = 1
    Dfun = Dfun.transpose()
    return Dfun

## Line shape functions
def Lineshape_RelOffset(p, pcov):
    """
    Relative offset (amplitude/offset) for line shape functions.
    """
    amp = p[1]
    y0 = p[2]
    rel_offset = y0/amp
    rel_offset_sigma = np.sqrt(
        (-2*y0*pcov[1, 2])/amp**3 + pcov[2, 2]/amp**2 + (y0**2*pcov[1, 1])/amp**4
        )

    return rel_offset, rel_offset_sigma

## Gaussian
def Gaussian(x, x0, amp, y0, gamma_g):
    """
    Gaussian normalized such that `amp` is the amplitude and `gamma_g` is the FWHM line width.
    """
    return amp * np.exp(-4*np.log(2)*(x-x0)**2/gamma_g**2) + y0

def Df_Gaussian(x, *p):
    """
    Partial derivatives for function `Gaussian`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_g = p[3]

    Dfun = np.zeros((4, len(x)))
    Dfun[0, :] = (
        amp * np.exp(-4*np.log(2)*(x-x0)**2/gamma_g**2)
        * (8*np.log(2)*(x-x0)/gamma_g**2))
    Dfun[1, :] = np.exp(-4*np.log(2)*(x-x0)**2/gamma_g**2)
    Dfun[2, :] = 1
    Dfun[3, :] = (
        amp * np.exp(-4*np.log(2)*(x-x0)**2/gamma_g**2)
        * (8*np.log(2)*(x-x0)**2/gamma_g**3))
    Dfun = Dfun.transpose()
    return Dfun

def Gaussian_Gamma(p, pcov):
    """
    Total FWHM line width and its uncertainty for function `Gaussian`.
    """
    return p[3], np.sqrt(pcov[3, 3])

def Gaussian_Area_Value(amp, gamma_g):
    """
    Total area and its uncertainty for function `Gaussian`.
    """
    area = amp*gamma_g*np.sqrt(np.pi/np.log(2))/2
    return area

def GaussianArea(x, x0, A, y0, gamma_g):
    """
    `Gaussian` normalized such that `A` is the area under the curve and `gamma_g` is the FWHM line
    width.
    """
    return (
        A * 2*np.sqrt(np.log(2))/np.sqrt(np.pi)/gamma_g
        * np.exp(-4*np.log(2)*(x-x0)**2/gamma_g**2) + y0)

def GaussianBeam(x, x0, w, amp, y0):
    """
    Gaussian normalized such that `amp` is the amplitude and `w` is the 1/e^2 radius.
    """
    return amp * np.exp(-2*(x-x0)**2/w**2) + y0

def Df_GaussianBeam(x, *p):
    """
    Partial derivatives for function `GaussianBeam`.
    """
    x0 = p[0]
    w = p[1]
    amp = p[2]

    Dfun = np.zeros((4, len(x)))
    Dfun[0, :] = amp * np.exp(-2*(x-x0)**2/w**2) * (4*(x-x0)/w**2)
    Dfun[1, :] = amp * np.exp(-2*(x-x0)**2/w**2) * (4*(x-x0)**2/w**3)
    Dfun[2, :] = np.exp(-2*(x-x0)**2/w**2)
    Dfun[3, :] = 1
    Dfun = Dfun.transpose()
    return Dfun

## Lorentzian
def Lorentzian(x, x0, amp, y0, gamma_l):
    """
    Lorentzian, normalized such that `amp` is the amplitude.
    """
    L = (amp * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2))
    return L + y0

def Df_Lorentzian(x, *p):
    """
    Partial derivatives for function `Lorentzian`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]

    L = (amp * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2))

    Dfun = np.zeros((4, len(x)))
    Dfun[0, :] = 4/gamma_l*L * ((x-x0)/amp/(gamma_l/2)*L)
    Dfun[1, :] = L/amp
    Dfun[2, :] = 1
    Dfun[3, :] = (2*L/gamma_l * (1-1*L/amp))
    Dfun = Dfun.transpose()
    return Dfun

def Lorentzian_Gamma(p, pcov):
    """
    Total FWHM line width and its uncertainty for function `Lorentzian`.
    """
    return p[3], np.sqrt(pcov[3, 3])

def LorentzianLinOffset(x, x0, amp, y0, gamma_l, s0):
    """
    Lorentzian, normalized such that `amp` is the amplitude and with linear offset with slope `s0`.
    """
    L = (amp * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2))
    return L + y0 + s0 * x

def Df_LorentzianLinOffset(x, *p):
    """
    Partial derivatives for function `LorentzianLinOffset`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]

    L = (amp * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2))

    Dfun = np.zeros((5, len(x)))
    Dfun[0, :] = 4/gamma_l*L * ((x-x0)/amp/(gamma_l/2)*L)
    Dfun[1, :] = L/amp
    Dfun[2, :] = 1
    Dfun[3, :] = (2*L/gamma_l * (1-1*L/amp))
    Dfun[4, :] = x
    Dfun = Dfun.transpose()
    return Dfun

def LorentzianArea(x, x0, A, y0, gamma_l):
    """
    Lorentzian, normalized such that `A` is the area under the curve.
    """
    return A * (gamma_l/2/np.pi) / ((x-x0)**2 + (gamma_l/2)**2) + y0

def Df_LorentzianArea(x, *p):
    """
    Partial derivatives for function `LorentzianArea`.
    """
    x0 = p[0]
    A = p[1]
    gamma_l = p[3]

    L = A * (gamma_l/2/np.pi) / ((x-x0)**2 + (gamma_l/2)**2)
    piAL = np.pi/A*L

    Dfun = np.zeros((4, len(x)))
    Dfun[0, :] = piAL*4*(x-x0)/gamma_l*L
    Dfun[1, :] = L/A
    Dfun[2, :] = 1
    Dfun[3, :] = (1/gamma_l - piAL)*L
    Dfun = Dfun.transpose()
    return Dfun

def Lorentzian_AtoAmp(gamma_l):
    """
    Convert the area under the curve to peak value for a Lorentzian with FWHM line width `gamma_l`.
    """
    return 2/np.pi/gamma_l

def Lorentzian_AmptoA(gamma_l):
    """
    Convert the peak value to area under the curve for a Lorentzian with FWHM line width `gamma_l`.
    """
    return np.pi*gamma_l/2

## Fano-Lorentzian
def FanoLorentzian(x, x0, amp, y0, gamma_l, eta):
    """
    Fano-Lorentzian (expanded line shape), normalized such that `amp` is the amplitude,
    with asymmetry parameter `eta` defined such that apparent line shift
    (i.e. difference in center frequency to a Lorentzian fit)
    is approximately `eta * gamma_l`.
    """
    L = (amp * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2))
    return L * (1 + 4*eta*(x-x0)/gamma_l) + y0

def Df_FanoLorentzian(x, *p):
    """
    Partial derivatives for function `FanoLorentzian`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]
    eta = p[4]

    L = (amp * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2))
    FL = L * (1 + 4*eta*(x-x0)/gamma_l)

    Dfun = np.zeros((5, len(x)))
    Dfun[0, :] = 4/gamma_l*L * ((x-x0)/amp/(gamma_l/2)*FL - eta)
    Dfun[1, :] = FL/amp
    Dfun[2, :] = 1
    Dfun[3, :] = (FL/gamma_l * (1-2*L/amp) + L/gamma_l)
    Dfun[4, :] = 4*(x-x0)/gamma_l*L
    Dfun = Dfun.transpose()
    return Dfun

def FanoLorentzianLinOffset(x, x0, amp, y0, gamma_l, eta, s0):
    """
    Fano-Lorentzian (expanded line shape), normalized such that `amp`
    is the amplitude and with linear offset with slope `s0`,
    with asymmetry parameter `eta` defined such that apparent line shift
    (i.e. difference in center frequency to a Lorentzian fit)
    is approximately `eta * gamma_l`.
    """
    L = (amp * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2))
    return L * (1 + 4*eta*(x-x0)/gamma_l) + y0 + s0 * x

def Df_FanoLorentzianLinOffset(x, *p):
    """
    Partial derivatives for function `FanoLorentzianLinOffset`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]
    eta = p[4]

    L = (amp * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2))
    FL = L * (1 + 4*eta*(x-x0)/gamma_l)

    Dfun = np.zeros((6, len(x)))
    Dfun[0, :] = 4/gamma_l*L * ((x-x0)/amp/(gamma_l/2)*FL - eta)
    Dfun[1, :] = FL/amp
    Dfun[2, :] = 1
    Dfun[3, :] = (FL/gamma_l * (1-2*L/amp) + L/gamma_l)
    Dfun[4, :] = 4*(x-x0)/gamma_l*L
    Dfun[5, :] = x
    Dfun = Dfun.transpose()
    return Dfun

def FanoLorentzianArea(x, x0, A, y0, gamma_l, eta):
    """
    Fano-Lorentzian (expanded line shape), normalized such that `A` is the area under the curve,
    with asymmetry parameter `eta` defined such that apparent line shift
    (i.e. difference in center frequency to a Lorentzian fit)
    is approximately `eta * gamma_l`.
    """
    return (A * (gamma_l/2/np.pi) / ((x-x0)**2 + (gamma_l/2)**2)) * (1 + eta*2*(2*(x-x0)/gamma_l)) + y0

def Df_FanoLorentzianArea(x, *p):
    """
    Partial derivatives for function `FanoLorentzianArea`.
    """
    x0 = p[0]
    A = p[1]
    gamma_l = p[3]
    eta = p[4]

    L = A * (gamma_l/2/np.pi) / ((x-x0)**2 + (gamma_l/2)**2)
    FL = L * (1 + eta*2*(2*(x-x0)/gamma_l))
    piAL = np.pi/A*L
    xx0GL = (x-x0)/gamma_l
    eGLL = eta/gamma_l*L

    Dfun = np.zeros((5, len(x)))
    Dfun[0, :] = (piAL*4*xx0GL*FL - 4*eGLL)
    Dfun[1, :] = FL/A
    Dfun[2, :] = 1
    Dfun[3, :] = ((1/gamma_l - piAL)*FL - 4*xx0GL*eGLL)
    Dfun[4, :] = 4*xx0GL*L
    Dfun = Dfun.transpose()
    return Dfun

def FanoLorentzianUli(x, x0, A, y0, gamma_l, a4, a1):
    """
    Fano-Lorentzian (expanded line shape)
    as defined by U. Jentschura.
    """
    return (A * (gamma_l/2)**2 / ((x-x0)**2 + (gamma_l/2)**2)) * (a1 + a4 * (x-x0)) + y0

## Voigt
def Voigt(x, x0, amp, y0, gamma_l, gamma_g):
    """
    Voigt, normalized such that `amp` is the amplitude.
    """
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    y = amp/scipy.special.wofz(1j*b).real * scipy.special.wofz(a+1j*b).real + y0
    return y

def Df_Voigt(x, *p):
    """
    Partial derivatives for function `Voigt`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]
    gamma_g = p[4]
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    h = scipy.special.wofz(a+1j*b).real
    Invw = 1/scipy.special.wofz(1j*b).real
    V = amp*Invw * scipy.special.wofz(a+1j*b).real
    H1 = (-2*a) * scipy.special.wofz(a+1j*b).real + (2*b) * scipy.special.wofz(a+1j*b).imag
    H2 = (2*b) * scipy.special.wofz(a+1j*b).real + (2*a) * scipy.special.wofz(a+1j*b).imag - 2/np.sqrt(np.pi)

    Dfun = np.zeros((5, len(x)))
    Dfun[0, :] = -amp*Invw*ca/gamma_g * H1
    Dfun[1, :] = V/amp
    Dfun[2, :] = 1
    Dfun[3, :] = amp*Invw*cb/gamma_g * (-2*b*h+2*h*Invw/np.sqrt(np.pi)+H2)
    Dfun[4, :] = amp*Invw/gamma_g**2 * (2*cb*gamma_l*h* (b-Invw/np.sqrt(np.pi)) - H1*ca*(x-x0) - H2*cb*gamma_l)
    Dfun = Dfun.transpose()
    return Dfun

def VoigtLinOffset(x, x0, amp, y0, gamma_l, gamma_g, s0):
    """
    Voigt, normalized such that `amp` is the amplitude and with linear offset with slope `s0`.
    """
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    y = amp/scipy.special.wofz(1j*b).real * scipy.special.wofz(a+1j*b).real + y0 + s0 * x
    return y

def Df_VoigtLinOffset(x, *p):
    """
    Partial derivatives for function `VoigtLinOffset`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]
    gamma_g = p[4]
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    h = scipy.special.wofz(a+1j*b).real
    Invw = 1/scipy.special.wofz(1j*b).real
    V = amp*Invw * scipy.special.wofz(a+1j*b).real
    H1 = (-2*a) * scipy.special.wofz(a+1j*b).real + (2*b) * scipy.special.wofz(a+1j*b).imag
    H2 = (2*b) * scipy.special.wofz(a+1j*b).real + (2*a) * scipy.special.wofz(a+1j*b).imag - 2/np.sqrt(np.pi)

    Dfun = np.zeros((6, len(x)))
    Dfun[0, :] = -amp*Invw*ca/gamma_g * H1
    Dfun[1, :] = V/amp
    Dfun[2, :] = 1
    Dfun[3, :] = amp*Invw*cb/gamma_g * (-2*b*h+2*h*Invw/np.sqrt(np.pi)+H2)
    Dfun[4, :] = amp*Invw/gamma_g**2 * (2*cb*gamma_l*h* (b-Invw/np.sqrt(np.pi)) - H1*ca*(x-x0) - H2*cb*gamma_l)
    Dfun[5, :] = x
    Dfun = Dfun.transpose()
    return Dfun

def VoigtArea(x, x0, A, y0, gamma_l, gamma_g):
    """
    Voigt, normalized such that `A` is the area under the curve.
    """
    c0 = 2*np.sqrt(np.log(2))/np.sqrt(np.pi)
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    y = A*c0/gamma_g * scipy.special.wofz(a+1j*b).real + y0
    return y

def Df_VoigtArea(x, *p):
    """
    Partial derivatives for function `VoigtArea`.
    """
    x0 = p[0]
    A = p[1]
    gamma_l = p[3]
    gamma_g = p[4]
    c0 = 2*np.sqrt(np.log(2))/np.sqrt(np.pi)
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    h = scipy.special.wofz(a+1j*b).real
    V = A*c0/gamma_g * h
    H1 = (-2*a) * scipy.special.wofz(a+1j*b).real + (2*b) * scipy.special.wofz(a+1j*b).imag
    H2 = (2*b) * scipy.special.wofz(a+1j*b).real + (2*a) * scipy.special.wofz(a+1j*b).imag - 2/np.sqrt(np.pi)

    Dfun = np.zeros((5, len(x)))
    Dfun[0, :] = -A*c0*ca/gamma_g**2 * H1
    Dfun[1, :] = V/A
    Dfun[2, :] = 1
    Dfun[3, :] = A*c0*cb/gamma_g**2 * H2
    Dfun[4, :] = -A*c0/gamma_g**2 * (h + H1*ca*(x-x0)/gamma_g + H2*cb*gamma_l/gamma_g)
    Dfun = Dfun.transpose()
    return Dfun

def Voigt_AtoAmp(gamma_l, gamma_g):
    """
    Convert the area under the curve to amplitude for a Voigt with Lorentzian FWHM linewidth
    `gamma_l` and Gaussian linewidth `gamma_g`.
    """
    c0 = 2*np.sqrt(np.log(2))/np.sqrt(np.pi)
    cb = np.sqrt(np.log(2))
    b = cb*gamma_l/gamma_g

    return c0/gamma_g * scipy.special.wofz(1j*b).real

def Voigt_AmptoA(gamma_l, gamma_g):
    """
    Convert the amplitude to area under the curve for a Voigt with Lorentzian FWHM linewidth
    `gamma_l` and Gaussian FWHM linewidth `gamma_g`.
    """
    c0 = 2*np.sqrt(np.log(2))/np.sqrt(np.pi)
    cb = np.sqrt(np.log(2))
    b = cb*gamma_l/gamma_g

    return gamma_g/c0/scipy.special.wofz(1j*b).real

def Voigt_Gamma_Value(gamma_l, gamma_g):
    """
    Approximate the total FWHM of the Voigt profile for a given Lorentzian FWHM linewidth `gamma_l`
    and Gaussian FWHM linewidth `gamma_g`.
    """
    return 0.5346*gamma_l + np.sqrt(0.2166*gamma_l**2+gamma_g**2)

def Voigt_Gamma_Sigma(gamma_l, gamma_g, sigmaGammaL, sigmaGammaG, r=-0.89):
    """
    Approximate the total FWHM linewidth of a Voigt for a given Lorentzian FHWM linewidth `gamma_l`
    and Gaussian FWHM linewidth `gamma_g`.
    Takes into account the correlation between `gamma_l` and `gamma_g` with the correlation
    coefficient `r`
    (default value for `r` of -0.89 was taken from 2S-4P measurement for an average of all delays).
    """
    a = 0.5346
    b = 0.2166
    dGdGL = a + b*gamma_l/np.sqrt(b*gamma_l**2+gamma_g**2)
    dGdGG = gamma_g/np.sqrt(b*gamma_l**2+gamma_g**2)
    sigma2 = (
        sigmaGammaL**2 * dGdGL**2
        + sigmaGammaG**2 * dGdGG**2
        + 2*dGdGL*dGdGG*sigmaGammaL*sigmaGammaG*r)
    return np.sqrt(sigma2)

def Voigt_Gamma(p, pcov):
    """
    Approximate the total FWHM linewidth and its uncertainty for function `Voigt`.
    """
    gamma_l = p[3]
    gamma_g = p[4]

    c1 = 0.5346
    c2 = 0.2166
    gamma = c1*gamma_l + np.sqrt(c2*gamma_l**2+gamma_g**2)

    gamma_sigma = np.sqrt(
        (
            2*gamma_g*(gamma_l*c2 + c1*np.sqrt(gamma_g**2 + gamma_l**2*c2))*pcov[3, 4]
            + (gamma_l*c2 + c1*np.sqrt(gamma_g**2 + gamma_l**2*c2))**2*pcov[3, 3]
            + gamma_g**2*pcov[4, 4])
        / (gamma_g**2 + gamma_l**2*c2)
        )
    return gamma, gamma_sigma

def Voigt_GammaG(Gamma, gamma_l):
    """
    Estimates the Gaussian FWHM linewidth for a Voigt from the total FWHM linewidth `Gamma`
    and the Lorentzian FWHM linewidth `gamma_l`.
    """
    Gamma2 = (Gamma-0.5346*gamma_l)**2-0.2166*gamma_l**2
    return np.sqrt(Gamma2) if Gamma2 > 0 else np.nan

def Voigt_GammaL(Gamma, gamma_g):
    """
    Estimates the Lorentzian FWHM linewidth for a Voigt from the total FWHM linewidth `Gamma`
    and the Gaussian FWHM linewidth `gamma_g`.
    """
    a = 0.5346
    b = 0.2166

    return (2*a*Gamma - np.sqrt(4*(a**2-b)*gamma_g**2+4*b*Gamma**2))/(2*(a**2-b))

## Voigt doublet
def VoigtDoublet(x, x1, amp1, y0, gamma_l, gamma_g, x2, amp2):
    """
    Voigt doublet, i.e. sum of two Voigts,
    normalized such that `Amp1` and `Amp2` are the amplitudes.
    """
    return (
        Voigt(x, x1, amp1, y0/2, gamma_l, gamma_g)
        + Voigt(x, x2, amp2, y0/2, gamma_l, gamma_g))

def Df_VoigtDoublet(x, *p):
    """
    Partial derivatives for function `VoigtDoublet`.
    """
    p1 = p[:5]
    p2 = np.hstack((p[5:], p[2:5]))
    Dfun_1 = Df_Voigt(x, *p1).transpose()
    Dfun_2 = Df_Voigt(x, *p2).transpose()

    Dfun = np.zeros((7, len(x)))
    Dfun[0, :] = Dfun_1[0, :]
    Dfun[1, :] = Dfun_1[1, :]
    Dfun[2, :] = Dfun_1[2, :]/2 + Dfun_2[2, :]/2
    Dfun[3, :] = Dfun_1[3, :] + Dfun_2[3, :]
    Dfun[4, :] = Dfun_1[4, :] + Dfun_2[4, :]
    Dfun[5, :] = Dfun_2[0, :]
    Dfun[6, :] = Dfun_2[1, :]
    Dfun = Dfun.transpose()
    return Dfun

def VoigtDoubletEqualAmps(x, x1, amp, y0, gamma_l, gamma_g, x2):
    """
    Voigt doublet, i.e. sum of two Voigts,
    normalized such that `amp` is the amplitude of both peaks.
    """
    return (
        Voigt(x, x1, amp, y0/2, gamma_l, gamma_g)
        + Voigt(x, x2, amp, y0/2, gamma_l, gamma_g))

def Df_VoigtDoubletEqualAmps(x, *p):
    """
    Partial derivatives for function `VoigtDoubletEqualAmps`.
    """
    p1 = p[:5]
    p2 = np.hstack((p[5:], p[1:5]))
    Dfun_1 = Df_Voigt(x, *p1).transpose()
    Dfun_2 = Df_Voigt(x, *p2).transpose()

    Dfun = np.zeros((6, len(x)))
    Dfun[0, :] = Dfun_1[0, :]
    Dfun[1, :] = Dfun_1[1, :] + Dfun_2[1, :]
    Dfun[2, :] = Dfun_1[2, :]/2 + Dfun_2[2, :]/2
    Dfun[3, :] = Dfun_1[3, :] + Dfun_2[3, :]
    Dfun[4, :] = Dfun_1[4, :] + Dfun_2[4, :]
    Dfun[5, :] = Dfun_2[0, :]
    Dfun = Dfun.transpose()
    return Dfun

def VoigtDoublet_CFR(p, pcov):
    """
    Find the weighted line center for function `VoigtDoublet`.
    """
    x1 = p[0]
    x2 = p[5]
    amp1 = p[1]
    amp2 = p[6]
    x0 = (amp1*x1+amp2*x2)/(amp1+amp2)

    x0_sigma = np.sqrt(
        (2*amp1*(x1/(amp1 + amp2) - (amp1*x1 + amp2*x2)/(amp1 + amp2)**2)*pcov[0, 1])/(amp1 + amp2)
        + (2*amp1*amp2*pcov[0, 5])/(amp1 + amp2)**2
        + (2*amp1*(x2/(amp1 + amp2) - (amp1*x1 + amp2*x2)/(amp1 + amp2)**2)*pcov[0, 6])/(amp1 + amp2)
        + (2*amp2*(x1/(amp1 + amp2) - (amp1*x1 + amp2*x2)/(amp1 + amp2)**2)*pcov[1, 5])/(amp1 + amp2)
        + 2*(x1/(amp1 + amp2) - (amp1*x1 + amp2*x2)/(amp1 + amp2)**2) * (x2/(amp1 + amp2) - (amp1*x1 + amp2*x2)/(amp1 + amp2)**2)*pcov[1, 6]
        + (2*amp2*(x2/(amp1 + amp2) - (amp1*x1 + amp2*x2)/(amp1 + amp2)**2)*pcov[5, 6])/(amp1 + amp2)
        + (amp1**2*pcov[0, 0])/(amp1 + amp2)**2
        + (x1/(amp1 + amp2) - (amp1*x1 + amp2*x2)/(amp1 + amp2)**2)**2*pcov[1, 1]
        + (amp2**2*pcov[5, 5])/(amp1 + amp2)**2
        + (x2/(amp1 + amp2) - (amp1*x1 + amp2*x2)/(amp1 + amp2)**2)**2*pcov[6, 6]
        )
    return x0, x0_sigma

def VoigtDoublet_UnweightedCFR(p, pcov):
    """
    Find the unweighted line center for function `VoigtDoublet`.
    """
    x1 = p[0]
    x2 = p[5]
    x0 = (x1+x2)/2

    x0_sigma = np.sqrt(
        pcov[0, 5]/2 + pcov[0, 0]/4 + pcov[5, 5]/4
        )
    return x0, x0_sigma

def VoigtDoublet_DeltaNu(p, pcov):
    """
    Find the frequency difference between the two peaks for function `VoigtDoublet`.
    """
    x1 = p[0]
    x2 = p[5]
    delta_nu = x2-x1

    delta_nu_sigma = np.sqrt(
        -2*pcov[0, 5] + pcov[0, 0] + pcov[5, 5]
        )
    return delta_nu, delta_nu_sigma

def VoigtDoublet_AmpRatio(p, pcov):
    """
    Find the ratio of the peak amplitudes for function `VoigtDoublet`.
    """
    x1 = p[0]
    x2 = p[5]
    amp1 = p[1]
    amp2 = p[6]

    if x2 >= x1:
        amp_ratio = amp1/amp2
        amp_ratio_sigma = np.sqrt(
            (-2*amp1*pcov[1, 6])/amp2**3 + pcov[1, 1]/amp2**2 + (amp1**2*pcov[6, 6])/amp2**4
            )
    else:
        amp_ratio = amp2/amp1
        amp_ratio_sigma = np.sqrt(
            (-2*amp2*pcov[1, 6])/amp1**3 + pcov[6, 6]/amp1**2 + (amp2**2*pcov[1, 1])/amp1**4
            )
    return amp_ratio, amp_ratio_sigma

def VoigtDoublet_AmpAvg(p, pcov):
    """
    Find the average of the peak amplitudes for function `VoigtDoublet`.
    """
    amp1 = p[1]
    amp2 = p[6]

    amp_avg = (amp1+amp2)/2
    amp_avg_sigma = np.sqrt(
        pcov[1, 1]/4 + pcov[6, 6]/4 + pcov[1, 6]/2
        )
    return amp_avg, amp_avg_sigma

def VoigtDoublet_RelOffset(p, pcov):
    """
    Relative offset (amplitude/offset) for function `VoigtDoublet`.
    """
    amp1 = p[1]
    amp2 = p[6]
    y0 = p[2]
    rel_offset = y0/(amp1+amp2)
    rel_offset_sigma = np.sqrt(
        (
            -2*(amp1 + amp2)*y0*pcov[1, 2] + 2*y0**2*pcov[1, 6]
            - 2*(amp1 + amp2)*y0*pcov[2, 6] + y0**2*pcov[1, 1]
            + (amp1 + amp2)**2*pcov[2, 2] + y0**2*pcov[6, 6])
        / (amp1 + amp2)**4
        )

    return rel_offset, rel_offset_sigma

def VoigtDoublet_AlphaDeltaNu(delta_nu_value, delta_nu_sigma, v_mean, freq):
    """
    Calculate angle alpha between atomic beam and laser beam from
    splitting `delta_nu_value` of the two Doppler-shifted peaks of function `VoigtDoublet`
    (and associated uncertainty `delta_nu_sigma`),
    using the mean velocitiy `v_mean` and the laser frequency `freq`.
    """
    coeff = (
        scipy.constants.c
        / (2*v_mean * freq))
    x = coeff * delta_nu_value
    alpha_delta_nu_value = np.arcsin(x)
    alpha_delta_nu_sigma = (
        delta_nu_sigma
        * (1/np.sqrt(1-x**2))*coeff
        )
    return alpha_delta_nu_value, alpha_delta_nu_sigma

## Fano-Voigt
def FanoVoigt(x, x0, amp, y0, gamma_l, gamma_g, eta):
    """
    Fano-Voigt, normalized such that `amp` is the amplitude,
    with asymmetry parameter `eta` defined such that apparent line shift
    (i.e. difference in center frequency to a Lorentzian/Voigt fit)
    is approximately `eta * Gamma`, where `Gamma` is the FWHM linewidth of the Voigt.
    """
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    y = amp/scipy.special.wofz(1j*b).real * (scipy.special.wofz(a+1j*b).real -2*eta*scipy.special.wofz(a+1j*b).imag) + y0
    return y

def Df_FanoVoigt(x, *p):
    """
    Partial derivatives for function `FanoVoigt`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]
    gamma_g = p[4]
    eta = p[5]
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    h = scipy.special.wofz(a+1j*b).real -2*eta*scipy.special.wofz(a+1j*b).imag
    Invw = 1/scipy.special.wofz(1j*b).real
    FV = amp*Invw * (scipy.special.wofz(a+1j*b).real -2*eta*scipy.special.wofz(a+1j*b).imag)
    H1 = (-2*a+4*eta*b) * scipy.special.wofz(a+1j*b).real + (2*b + 4*eta*a) * scipy.special.wofz(a+1j*b).imag - 4*eta/np.sqrt(np.pi)
    H2 = (2*b+4*eta*a) * scipy.special.wofz(a+1j*b).real + (2*a - 4*eta*b) * scipy.special.wofz(a+1j*b).imag - 2/np.sqrt(np.pi)

    Dfun = np.zeros((6, len(x)))
    Dfun[0, :] = -amp*Invw*ca/gamma_g * H1
    Dfun[1, :] = FV/amp
    Dfun[2, :] = 1
    Dfun[3, :] = amp*Invw*cb/gamma_g * (-2*b*h+2*h*Invw/np.sqrt(np.pi)+H2)
    Dfun[4, :] = amp*Invw/gamma_g**2 * (2*cb*gamma_l*h* (b-Invw/np.sqrt(np.pi)) - H1*ca*(x-x0) - H2*cb*gamma_l)
    Dfun[5, :] = amp*Invw * (-2 * scipy.special.wofz(a+1j*b).imag)
    Dfun = Dfun.transpose()
    return Dfun

def FanoVoigtLinOffset(x, x0, amp, y0, gamma_l, gamma_g, eta, s0):
    """
    Fano-Voigt, normalized such that `amp` is the amplitude and with linear offset with slope `s0`,
    with asymmetry parameter `eta` defined such that apparent line shift
    (i.e. difference in center frequency to a Lorentzian/Voigt fit)
    is approximately `eta * Gamma`, where `Gamma` is the FWHM linewidth of the Voigt.
    """
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    y = (
        amp/scipy.special.wofz(1j*b).real
        * (scipy.special.wofz(a+1j*b).real - 2*eta*scipy.special.wofz(a+1j*b).imag)
        + y0 + s0 * x)
    return y

def Df_FanoVoigtLinOffset(x, *p):
    """
    Partial derivatives for function `FanoVoigtLinOffset`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]
    gamma_g = p[4]
    eta = p[5]
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    h = scipy.special.wofz(a+1j*b).real -2*eta*scipy.special.wofz(a+1j*b).imag
    Invw = 1/scipy.special.wofz(1j*b).real
    FV = amp*Invw * (scipy.special.wofz(a+1j*b).real -2*eta*scipy.special.wofz(a+1j*b).imag)
    H1 = (
        (-2*a+4*eta*b) * scipy.special.wofz(a+1j*b).real
        + (2*b + 4*eta*a) * scipy.special.wofz(a+1j*b).imag
        - 4*eta/np.sqrt(np.pi))
    H2 = (
        (2*b+4*eta*a) * scipy.special.wofz(a+1j*b).real
        + (2*a - 4*eta*b) * scipy.special.wofz(a+1j*b).imag
        - 2/np.sqrt(np.pi))

    Dfun = np.zeros((7, len(x)))
    Dfun[0, :] = -amp*Invw*ca/gamma_g * H1
    Dfun[1, :] = FV/amp
    Dfun[2, :] = 1
    Dfun[3, :] = amp*Invw*cb/gamma_g * (-2*b*h+2*h*Invw/np.sqrt(np.pi)+H2)
    Dfun[4, :] = (
        amp*Invw/gamma_g**2
        * (2*cb*gamma_l*h* (b-Invw/np.sqrt(np.pi)) - H1*ca*(x-x0) - H2*cb*gamma_l))
    Dfun[5, :] = amp*Invw * (-2 * scipy.special.wofz(a+1j*b).imag)
    Dfun[6, :] = x
    Dfun = Dfun.transpose()
    return Dfun

def FanoVoigtArea(x, x0, A, y0, gamma_l, gamma_g, eta):
    """
    Fano-Voigt, normalized such that `A` is the area under the curve,
    with asymmetry parameter `eta` defined such that apparent line shift
    (i.e. difference in center frequency to a Lorentzian/Voigt fit)
    is approximately `eta * Gamma`, where `Gamma` is the FWHM linewidth of the Voigt.
    """
    c0 = 2*np.sqrt(np.log(2))/np.sqrt(np.pi)
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    y = (
        A*c0/gamma_g * (scipy.special.wofz(a+1j*b).real - 2*eta*scipy.special.wofz(a+1j*b).imag) + y0)
    return y

def Df_FanoVoigtArea(x, *p):
    """
    Partial derivatives for function `FanoVoigtArea`.
    """
    x0 = p[0]
    A = p[1]
    gamma_l = p[3]
    gamma_g = p[4]
    eta = p[5]
    c0 = 2*np.sqrt(np.log(2))/np.sqrt(np.pi)
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    h = scipy.special.wofz(a+1j*b).real -2*eta*scipy.special.wofz(a+1j*b).imag
    FV = A*c0/gamma_g * h
    H1 = (
        (-2*a+4*eta*b) * scipy.special.wofz(a+1j*b).real
        + (2*b + 4*eta*a) * scipy.special.wofz(a+1j*b).imag
        - 4*eta/np.sqrt(np.pi))
    H2 = (
        (2*b+4*eta*a) * scipy.special.wofz(a+1j*b).real
        + (2*a - 4*eta*b) * scipy.special.wofz(a+1j*b).imag
        - 2/np.sqrt(np.pi))

    Dfun = np.zeros((6, len(x)))
    Dfun[0, :] = -A*c0*ca/gamma_g**2 * H1
    Dfun[1, :] = FV/A
    Dfun[2, :] = 1
    Dfun[3, :] = A*c0*cb/gamma_g**2 * H2
    Dfun[4, :] = -A*c0/gamma_g**2 * (h + H1*ca*(x-x0)/gamma_g + H2*cb*gamma_l/gamma_g)
    Dfun[5, :] = A*c0/gamma_g * (-2 * scipy.special.wofz(a+1j*b).imag)
    Dfun = Dfun.transpose()
    return Dfun

## Maxwell-Boltzmann distribution
def GenMaxwellBoltzmann(v, n, amp, v0):
    """
    Generalized Maxwell-Boltzmann distribution,
    normalized such that `amp` is the amplitude.
    """
    return (
        amp*v**n*np.exp(-v**2/v0**2)
        /(
            2**(-n/2)*np.exp(-n/2)
            *(np.sqrt(np.abs(n))*v0)**n))

def GenMaxwellBoltzmannCutoff(v, n, amp, v0, vcut):
    """
    Generalized Maxwell-Boltzmann distribution with cutoff `vcut` for low speeds.
    """
    return (
        amp*v**n*np.exp(-v**2/v0**2)*np.exp(-vcut/v)
        /(
            2**(-n/2)*np.exp(-n/2)
            *(np.sqrt(np.abs(n))*v0)**n))

## Error function
def ErrorFctBeamRadius(x, x0, A, w, y0):
    """
    Complimentary error function, corresponding to integral of Gaussian
    with `w` as 1/e^2 radius.
    """
    y = A/2*(scipy.special.erfc(-np.sqrt(2)/w*(x-x0))) + y0
    return y

def Df_ErrorFctBeamRadius(x, *p):
    """
    Partial derivatives for function `ErrorFctBeamRadius`.
    """
    Dfun = np.zeros((4, len(x)))
    Dfun[0, :] = p[1]/np.sqrt(np.pi)*np.exp(-2/p[2]**2*(x-p[0])**2)*-np.sqrt(2)/p[2]
    Dfun[1, :] = (ErrorFctBeamRadius(x, *p)-p[3])/p[1]
    Dfun[2, :] = p[1]/np.sqrt(np.pi)*np.exp(-2/p[2]**2*(x-p[0])**2)*-np.sqrt(2)/p[2]**2*(x-p[0])
    Dfun[3, :] = 1
    Dfun = Dfun.transpose()
    return Dfun

## Trigonometric functions
def Sine(x, A, omega, x0, y0):
    """Sine."""
    return A * np.sin(omega*(x-x0)) + y0

def SineDeg(x, A, omega, x0, y0):
    """Sine using degrees instead of radian."""
    return A * np.sin(2*np.pi*omega*(x-x0)) + y0

## Quantum interference
def QIJ12_MagicAngle(p, pcov):
    """
    Find the quantum interference magic angle for J=1/2,
    i.e. the zero crossing for falling slope.
    """
    A = p[0]
    x0 = p[1]
    y0 = p[2]
    omega_ = np.pi/90

    if A > 0:
        A = -p[0]
        x0 = np.mod(p[1], 90)

    theta_magic_value = x0 + (1/omega_)*(np.arcsin(-y0/A))

    theta_magic_sigma = np.sqrt(
        (
         2*A**2*y0*np.sqrt(1 - y0**2/A**2)*omega_*pcov[0, 1]
         - 2*A*y0*pcov[0, 2]
         - 2*A**3*np.sqrt(1 - y0**2/A**2)*omega_*pcov[1, 2]
         + y0**2*pcov[0, 0]
         + A**4*omega_**2*pcov[1, 1]
         - A**2*y0**2*omega_**2*pcov[1, 1]
         + A**2*pcov[2, 2]
         )
        / (A**2*(A**2 - y0**2)*omega_**2)
        )

    return theta_magic_value, theta_magic_sigma
