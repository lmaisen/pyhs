# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Various statistical functions.

Literature:
PDG2018:
Tanabashi, M., & others. (2018). Particle data group, ch. 39. Phys. Rev. D, 98(3), 30001.
NR3:
Press, W. H., Teukolsky, S. A., Vetterling, W. T., & Flannery, B. P. (2007).
Numerical Recipes: The Art of Scientific Computing (3rd ed.). Cambridge University Press.
DataPlotWSD:
NIST Dataplot Reference Manual, LET Subcommands and Library Functions, Statistics LET Subcommands,
https://www.itl.nist.gov/div898/software/dataplot/refman2/ch2/weightsd.pdf
"""

import numpy as np
from scipy.stats import chi2
from scipy.stats import norm
from scipy.stats import t

@np.deprecate(new_name='avg_uncorr_meas')
def avgUncorrMeas(sampleData, sampleSigma, axis=0):
    """
    Deprecated, replaced by `avg_uncorr_meas`:
    Weighted average of values `a` with associated uncertainties `sa`.
    The values in `a` are assumed to be uncorrelated.
    """
    sampleData = np.array(sampleData)
    sampleSigma = np.array(sampleSigma)
    if sampleData.shape[axis] > 0:
        sigma2 = 1/np.sum(1/sampleSigma**2, axis=axis)
        mean = np.sum(sampleData/sampleSigma**2, axis=axis)*sigma2
        sigma = np.sqrt(sigma2).squeeze()
        chiSq = (
            np.sum((sampleData-mean)**2/sampleSigma**2, axis=axis)
            .squeeze())
        mean = mean.squeeze()
        if len(sampleData) > 1:
            redChiSq = chiSq/(len(sampleData)-1)
        else:
            redChiSq = chiSq
    else:
        mean = np.nan
        sigma = np.nan
        chiSq = np.nan
        redChiSq = np.nan

    return dict(Mean=mean, Sigma=sigma, ChiSq=chiSq, RedChiSq=redChiSq)

@np.deprecate(new_name='avg_uncorr_meas2')
def avgUncorrMeas2(sampleData, sampleSigma):
    """
    Deprecated, replaced by `avg_uncorr_meas2`:
    Same as `avgUncorrMeas`, but not returning dict by default.
    """
    res = avgUncorrMeas(sampleData, sampleSigma)

    return np.array([res['Mean'], res['Sigma'], res['ChiSq'], res['RedChiSq']])

@np.deprecate(new_name='add_mean')
def addMean(a, b, wa, wb):
    """
    Deprecated, replaced by `add_mean`.
    """
    return add_mean(a, b, wa, wb)

def add_mean(a, b, wa, wb):
    """
    Add mean values `a` and `b` with weights `wa` and `wb`.
    """
    return wa*a+wb*b

@np.deprecate(new_name='add_uncert')
def addUncert(sa, sb, r, wa, wb):
    """
    Deprecated, replaced by `add_uncert`.
    """
    return add_uncert(sa, sb, r, wa, wb)

def add_uncert(sa, sb, r, wa, wb):
    """
    Add (lists/arrays of) uncertainties `sa` and `sb` with correlation coefficients `r`
    between `sa` und `sb` with (lists/arrays of) weights `wa` and `wb`.
    The elements in `sa` (`sb`) are assumed to be not correlated.
    """
    sa = np.atleast_1d(sa)
    sb = np.atleast_1d(sb)
    wa = np.atleast_1d(wa)
    wb = np.atleast_1d(wb)
    r = np.atleast_1d(r)
    if len(r) == 1:
        r0 = r
        r = np.empty((len(sa)))
        r[:] = r0
    s = np.sqrt(wa**2*sa**2+wb**2*sb**2+2*wa*wb*sa*sb*r)
    if len(s) == 1:
        s = s[0]
    return s

@np.deprecate(new_name='uncert_opt_weight')
def uncertOptWeight(sa, sb, r):
    """
    Deprecated, replaced by `uncert_opt_weight`.
    """
    return uncert_opt_weight(sa, sb, r)

def uncert_opt_weight(sa, sb, r):
    """
    Find optimal weights (with `b = 1-a`) for a weighted mean
    with uncertainties `sa` and `sb` and correlation coefficients `r`.
    """
    covab = sa*sb*r
    a = np.sum(sb**2-covab)/np.sum(sa**2+sb**2-2*covab)
    return a

def avg_uncorr_meas(a, sa=None, weights=None, axis=0, return_dict=True, full_output=True,
                    squeeze=True):
    """
    Weighted average of values `a` (list-like) with optional associated uncertainties `sa`
    (list-like), using weights `weights` (list-like) if supplied.
    If neither uncertainties nor weights are supplied, a simple average is performed.
    If only weights are supplied, but no uncertainties, a weighted average is performed and
    and the mean is returned, but no total uncertainty, chi-squared, ... is returned.
    If only uncertainties are supplied, but no weights, a weighted average is with weights
    given by 1/uncertainties^2 is performed, and mean, total uncertainty, chi-squared, ...
    is returned. This weighted average corresponds to the combination of values `a` with
    the lowest chi-squared and lowest total uncertainty.
    If both uncertainties and weights are supplied, a weighted average using the weights is
    performed and the mean returned. The total uncertainty, chi-squared, ... is calculated
    using the weights and uncertainties.
    The values in `a` are assumed to be uncorrelated.

    If `a`, `sa`, and `weights` are 2-D N x M arrays, M weighted averages of N values are performed
    and length-M 1-D arrays are returned. For the special case of a 2-D N x 1 array, be default
    zero length arrays are returned. If `squeeze` is set to False, length-1 1-D arrays are returned
    instead.
    """
    a = np.atleast_1d(a)
    sa = np.atleast_1d(sa) if sa is not None else None
    weights = np.atleast_1d(weights) if weights is not None else None

    mean = np.nan
    sigma = np.nan
    chi_sq = np.nan
    red_chi_sq = np.nan
    chi_sq_sigma = np.nan
    red_chi_sq_sigma = np.nan
    p_value = np.nan
    significance = np.nan
    num_points = len(a)
    if weights is None and sa is not None:
        # Weights not supplied, use 1/uncertainties^2 as weights (this corresponds to a
        # weighted average with minimal chi-squared and minimum total uncertainty)
        weights = 1/sa**2
    if a.shape[axis] > 0 and weights is None:
        # No weights or uncertainties supplied, do simple average
        mean = np.mean(a, axis=axis)
        if len(a.shape) > 1:
            sigma = np.repeat(sigma, a.shape[1])
            chi_sq = np.repeat(chi_sq, a.shape[1])
            red_chi_sq = np.repeat(red_chi_sq, a.shape[1])
            num_points = np.repeat(num_points, a.shape[1])
            chi_sq_sigma = np.repeat(chi_sq_sigma, a.shape[1])
            red_chi_sq_sigma = np.repeat(red_chi_sq_sigma, a.shape[1])
            p_value = np.repeat(p_value, a.shape[1])
            significance = np.repeat(significance, a.shape[1])
    elif a.shape[axis] > 0:
        # Do weighted average
        weights_normalization = 1/np.sum(weights, axis=axis)
        mean = np.sum(a*weights, axis=axis)*weights_normalization
        if sa is None:
            # No uncertainties supplied, do not assign total uncertainty
            sigma = np.nan
        else:
            # Calculate total uncertainty
            # For the case of uncertainties = weights (`sa == weights`), this reduces to
            # `sigma = np.squeeze(np.sqrt(sigma2))`
            sigma = (
                np.sqrt(np.sum(weights**2*sa**2, axis=axis)*weights_normalization**2))
            if len(sigma.shape) > 0:
                num_points = np.repeat(num_points, sigma.shape[0])
                dof = np.repeat(len(a)-1, sigma.shape[0])
            else:
                dof = len(a)-1
            chi_sq = (
                np.sum((a-mean)**2/sa**2, axis=axis))
            chi_sq_sigma = np.sqrt(2*dof)
            if len(a) > 1:
                red_chi_sq = chi_sq/dof
                red_chi_sq_sigma = chi_sq_sigma/dof
            else:
                red_chi_sq = chi_sq
                red_chi_sq_sigma = chi_sq_sigma
            p_value = get_p_value_from_chi_sq(chi_sq, dof)
            significance = get_significance(p_value)

    if len(a.shape) > 1 and squeeze:
        mean = np.squeeze(mean)
        sigma = np.squeeze(sigma)
        chi_sq = np.squeeze(chi_sq)
        red_chi_sq = np.squeeze(red_chi_sq)
        num_points = np.squeeze(num_points)
        chi_sq_sigma = np.squeeze(chi_sq_sigma)
        red_chi_sq_sigma = np.squeeze(red_chi_sq_sigma)
        p_value = np.squeeze(p_value)
        significance = np.squeeze(significance)

    if return_dict:
        output = {
            'Mean': mean,
            'Sigma': sigma,
            'ChiSq': chi_sq,
            'RedChiSq': red_chi_sq,
            }
        if full_output:
            output = {
                **output,
                'NPoints': num_points,
                'ChiSqSigma': chi_sq_sigma,
                'RedChiSqSigma': red_chi_sq_sigma,
                'PValue': p_value,
                'Significance': significance,
                }
    else:
        if full_output:
            output = np.array([
                mean, sigma,
                chi_sq, red_chi_sq,
                num_points,
                chi_sq_sigma, red_chi_sq_sigma,
                p_value, significance,
                ])
        else:
            output = np.array([
                mean, sigma,
                chi_sq, red_chi_sq])

    return output

def avg_uncorr_meas2(a, sa=None, axis=0, return_dict=False, full_output=False, **kwargs):
    """
    Same as `avg_uncorr_meas`, but not returning dict by default.
    """
    return avg_uncorr_meas(
        a, sa=sa, axis=axis, return_dict=return_dict, full_output=full_output, **kwargs)

def avg_corr_meas(a, b, sa, sb, r, return_dict=False, wa=None):
    """
    Average N pairs of values `a` and `b` with associated uncertainties `sa` and `sb`,
    taking into account the correlation (given by the linear correlation
    coefficient `r`) between the values in `a` and in `b`. The values within `a` or `b`
    are assumed to be not correlated.
    I.e., values `a[i]` and `b[j]` are correlated with `r` for `i = j`, while `r = 0`
    for `i != j`.
    The weights of the elements of `a` can be supplied as keyword argument `wa` (list-like),
    either as a single value (or 1-element array) that applies to all N values, or N values.
    Otherwise, they are determined for each element such that the combined uncertainty is minimized.
    The weights are normalized, that is, the weights `wb` of the elements of `b` are `1-wa`.
    """
    a = np.atleast_1d(a)
    b = np.atleast_1d(b)
    sa = np.atleast_1d(sa)
    sb = np.atleast_1d(sb)
    r = np.atleast_1d(r)

    if wa is None:
        # Find optimal weights
        if np.all(sa == sb):
            wa = np.array([0.5])
        else:
            covab = sa*sb*r
            wa = (sb**2-covab)/(sa**2+sb**2-2*covab)
    else:
        wa = np.atleast_1d(wa)
    wb = 1-wa

    # Combined uncertainty
    sc = np.sqrt(wa**2*sa**2+wb**2*sb**2+2*wa*wb*sa*sb*r)

    # Weighted average
    c = wa*a+wb*b

    if len(a) == 1:
        if return_dict:
            output = {
                'Mean': c[0], 'Sigma': sc[0],
                'WeightA': wa[0], 'WeightB': wb[0],
                }
        else:
            output = c[0], sc[0], wa[0], wb[0]
    else:
        output = c, sc, wa, wb
    return output

def weighted_std(values, weights=None, ddof=0):
    """
    Weighted standard deviation of values `values` (list-like) using weights `weights` (list-like).
    If `weights` is set to None (default), the unweighted standard deviation is returned.
    For the delta degrees of freedom `ddof` (int) set to 0 (default), the population standard
    deviation is returned. For `ddof` set to 1, the sample standard deviation is returned.
    See reference DataPlotWSD.
    """
    # Weighted mean of values
    weighted_mean = avg_uncorr_meas(values, weights=weights)['Mean']
    values = np.atleast_1d(values)
    if weights is None:
        # No weights given, use unweighted standard deviation
        wsd = np.std(values, ddof=ddof)
    elif len(values) <= ddof:
        # Delta degree of freedoms equal or larger than number of samples, return NaN
        wsd = np.nan
    else:
        # Calculate weighted standard deviation
        weights = np.atleast_1d(weights)
        # Number of nonzero weights
        num_nonzero_weights = (weights != 0.).sum()
        wsd = np.sqrt(
            num_nonzero_weights*np.sum(weights*(values-weighted_mean)**2)
            /((num_nonzero_weights-ddof)*np.sum(weights))
            )
    return wsd

def get_cov_r(x, y):
    """
    Get covariance and linear correlation coefficient (Pearson's r)
    for N measurements of `x` and `y`.
    """
    if len(x) < 2:
        xy_cov = np.full((2, 2), np.nan)
    else:
        xy_cov = np.cov(np.vstack((x, y)))
    r_cov = xy_cov[1, 0]/np.sqrt(xy_cov[0, 0]*xy_cov[1, 1])
    return xy_cov[1, 0], r_cov

def scale_by_rcs(y_sigma, red_chi_sq, scale_by_rcs=True):
    """
    Scale uncertainty ndarray `y_sigma` by square root of red. χ^2 in
    ndarray `red_chi_sq`, if and only if red. χ^2 is above 1.
    Use sparingly.
    """
    y_sigma = np.atleast_1d(y_sigma).astype(float)
    if scale_by_rcs:
        red_chi_sq = np.atleast_1d(red_chi_sq)
        mask_red_chi_sq = red_chi_sq > 1
        y_sigma[mask_red_chi_sq] = (
            y_sigma[mask_red_chi_sq] * np.sqrt(red_chi_sq[mask_red_chi_sq]))
    return y_sigma

def get_p_value_from_chi_sq(chi_sq, dof):
    """
    Get single-sided p-value from chi^2 `chi_sq` and DOF `dof`,
    see PDG2018, Eq. (39.71).
    This is the probability of finding a chi^2 at least as large as `chi_sq`,
    assuming the chi^2 distribution with DOF `dof` describes the data.
    """
    return 1-chi2.cdf(chi_sq, dof)

def get_p_value_of_r(r, num_samples):
    """
    Get double-sided p-value for linear correlation coefficient `r` for
    number of samples `num_samples`. The p-value is the probability to find a linear correlation
    coefficient at least at as large the given `r` for uncorrelated data, independent
    of the sign of `r` (hence double-sided).
    Only valid if the data can be described with a two-dimensional Gaussian.
    See NR3, Section 14.5.
    """
    # Statistic that is distributed in the null case (no correlation) like
    # Student's t-distribution with DOF dof, see NR3, Eq. (14.5.5)
    dof = num_samples-2
    if np.abs(r) == 1.:
        t_ = np.inf
    else:
        t_ = r*np.sqrt((num_samples-2)/(1-r**2))

    # Double-sided p-value for Student's t-distribution,
    # given by 1-A(t|nu) with A(t|nu) as given in NR3, Eq. (6.14.11)
    p_value = 2*(1-t.cdf(t_, dof))

    return p_value

def get_significance(p_value):
    """
    Get the single-sided significance Z associated with the p-value `p_value`,
    see PDG2018, Eq. (39.46):
    "Often one converts the p-value into an equivalent significance Z,
    defined so that a Z standard deviation upward fluctuation of a
    Gaussian random variable would have an upper tail area equal to p."
    I.e. the value Z for which the area of a normal distribution with standard
    deviation 1 integrated within -infinity - +Z is equal to 1-`p_value`.
    """
    return norm.isf(p_value)

def get_significance_double_sided(p_value):
    """
    Get the double-sided significance Z associated with the p-value `p_value`,
    i.e. the value Z for which the area of a normal distribution with standard
    deviation 1 integrated within -Z - +Z is equal to 1-`p_value`.
    """
    return norm.isf(p_value/2)

@np.deprecate(new_name='compact_uncert_str')
def compactUncertStr(value, error, sigErrDigits=None, expNot=False, tex=True):
    """
    Deprecated, replaced by `compact_uncert_str`.
    """
    return compact_uncert_str(
        value, error, sig_err_digits=sigErrDigits, exp_notation=expNot, tex=tex)

def compact_uncert_str(value, error, sig_err_digits=None, exp_notation=False, tex=True,
                       sigErrDigits=None, expNot=None, thousand_sep=None):
    """
    Convert a measurement value float `value` with associated uncertainty
    float `error` into a compact uncertainty string, with the uncertainty in the
    last digits given in parenthesis. The number of digits of the uncertainty
    given is set by int `sig_err_digits`.
    Exponential notation can be switched on with bool `exp_notation`.
    A thousand separator can be specified with `thousand_sep`. If a str is supplied, this is used
    as thousand separator. If set to True, ',' is used. If set to None (default) or other,
    no separator is used.
    `sigErrDigits` and `expNot` are the deprecated synonyms of `sig_err_digits` and `exp_notation`
    and will be removed in future versions.
    """
    if sigErrDigits is not None:
        # Legacy parameter provided
        sig_err_digits = sigErrDigits
        print(
            '`compact_uncert_str`: Argument `sigErrDigits` is deprecated, '
            +'use `sig_err_digits` instead')
    elif sig_err_digits is None:
        sig_err_digits = 1
    sig_err_digits = int(sig_err_digits)
    if expNot is not None:
        # Legacy parameter provided
        exp_notation = expNot
        print(
            '`compact_uncert_str`: Argument `expNot` is deprecated, '
            +'use `exp_notation` instead')
    notation = 'f'
    if any([np.isnan(value), np.isnan(error)]):
        output = 'NaN(NaN)'
    else:
        error = np.abs(error)
        if exp_notation:
            if value == 0.:
                scale = 1
            else:
                scale = int(np.floor(np.log10(np.abs(value))))
            scale_str = (
                r'$\times$10$^{{{}}}$'.format(scale) if tex
                else 'E{:d}'.format(scale))
            value = value*10**-scale
            error = error*10**-scale
        else:
            scale = 1
            scale_str = ''
        if error == 0.:
            digits = sig_err_digits-1
        else:
            digits = -int(np.floor(np.log10(error)))+sig_err_digits-1
            error_ = np.round(error, digits)
            digits = -int(np.floor(np.log10(error_)))+sig_err_digits-1
        thousand_sep_str = (
            thousand_sep if isinstance(thousand_sep, str)
            else (
                ',' if (isinstance(thousand_sep, bool) and thousand_sep) else ''))
        if digits < 0:
            output = (
                '{1:{5}.{3}{0}}({2:{5}.0{0}}){4}'
                .format(notation, value, error, 0, scale_str, thousand_sep_str))
        else:
            if np.round(error, digits) >= 1:
                output = (
                    '{1:{6}.{3}{0}}({2:{6}.{4}{0}}){5}'
                    .format(
                        notation, value, error,
                        digits, digits, scale_str, thousand_sep_str))
            else:
                output = (
                    '{1:{5}.{3}{0}}({2:{5}.0{0}}){4}'
                    .format(
                        notation, value, error*10**digits,
                        digits, scale_str, thousand_sep_str))
    return output

def format_number(value, sig_digits=None,
                  exp_not=False, tex=True, auto_exp_not=True):
    """
    Convert a measurement value float `value` into a string.
    The number of digits of the value given is set by int `sig_digits`.
    Exponential notation can be switched on with bool `exp_not`.
    """
    error = value
    if sig_digits is None:
        sig_digits = 1
    else:
        sig_digits = int(sig_digits)
    notation = 'f'
    if np.isnan(value):
        output = 'NaN'
    else:
        if auto_exp_not and (
                value > 10**sig_digits or value < 10**-(sig_digits-1)):
            exp_not = True
        error = np.abs(error)
        if exp_not:
            if value == 0.:
                scale = 1
            else:
                scale = int(np.floor(np.log10(np.abs(value))))
            scale_str = (
                r'$\times$10$^{{{}}}$'.format(scale) if tex
                else 'E{:d}'.format(scale))
            value = value*10**-scale
            error = error*10**-scale
        else:
            scale = 1
            scale_str = ''
        if error == 0.:
            digits = sig_digits-1
        else:
            digits = -int(np.floor(np.log10(error)))+sig_digits-1
            error_ = np.round(error, digits)
            digits = -int(np.floor(np.log10(error_)))+sig_digits-1
        if digits < 0:
            output = (
                '{1:.{2}{0}}{3}'
                .format(notation, value, 0, scale_str))
        else:
            if np.round(error, digits) >= 1:
                output = (
                    '{1:.{2}{0}}{3}'
                    .format(
                        notation, value,
                        digits, scale_str))
            else:
                output = (
                    '{1:.{2}{0}}{3}'
                    .format(
                        notation, value,
                        digits, scale_str))
    return output
