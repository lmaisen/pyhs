# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Various functions concerning time conversions.
"""

import datetime
import numpy as np
import pandas as pd
import astropy
import astropy.time
import scipy.interpolate

def convert_time(time, format_in='auto', format_out='mjd'):
    """
    Convert time `time` of given format `format_in` to format `format_out`.
    Available input formats of `time` as determined by `format_in` are:
    - 'datetime': `datetime.date` or `pd.Timestamp`
    - 'datetime64': `np.datetime64`
    - 'mjd': Modified Julian date, given as float
    - 'auto': try to determine format automatically.
    Available outout formats as determined by `format_out` include:
    - 'datetime': `pd.Timestamp`
    - 'datetime64': `np.datetime64`
    - 'mjd': Modified Julian date, given as float.
    """
    if format_in == 'auto':
        if isinstance(time, datetime.date):
            format_ = 'datetime'
        elif isinstance(time, np.datetime64):
            format_ = 'datetime64'
        else:
            format_ = 'mjd'
    else:
        format_ = format_in
    astropy_time = astropy.time.Time(time, format=format_)
    astropy_time.format = format_out
    time_conv = astropy_time.value
    if format_out == 'datetime':
        time_conv = pd.to_datetime(time_conv)
    return time_conv

def convert_times(times, format_in='auto', format_out='mjd'):
    """
    Same as `convert_time`, but accepting multiple times `times` (list-like) and returning a
    1D array.
    """
    times = np.atleast_1d(times)

    # Auto-determine format of incoming time data, can be mixed datetime and MJD as well
    if format_in == 'auto':
        if np.all(list(set(map(lambda i: isinstance(i, datetime.date), times)))):
            astropy_times = astropy.time.Time(times, format='datetime')
        elif np.all(list(set(map(lambda i: isinstance(i, np.datetime64), times)))):
            astropy_times = astropy.time.Time(times, format='datetime64')
        elif np.issubdtype(times.dtype, np.number):
            astropy_times = astropy.time.Time(times, format='mjd')
        else:
            times_mjd = [
                convert_time(time, format_in='auto', format_out='mjd')
                for time in times]
            astropy_times = astropy.time.Time(times_mjd, format='mjd')
    # Format of incoming time data passed as argument
    else:
        astropy_times = astropy.time.Time(times, format=format_in)
    astropy_times.format = format_out
    times_conv = astropy_times.value
    if format_out == 'datetime':
        times_conv = pd.to_datetime(times_conv)
    return times_conv

def get_uniform_timebase(t_min, t_max, freq='1s'):
    """
    Get array of times between times `t_min` and `t_max` with spacing freq,
    constituting a uniform timebase.
    """
    uniform_timebase = pd.date_range(
        t_min, t_max, freq=freq)
    return np.asarray(uniform_timebase)

def match_timebase(t1, t2, y2, fill_outside=False):
    """
    Find values of pairs of points (`t2`, `y2`) at times `t1`, using linear interpolation.
    If `fill_outside` is True, values outside the range of `t2` will take the
    value of the edge values of `t2`. If `fill_outside` is False (default), `np.nan` will be
    used instead.
    """
    # Convert times to float to allow interpolation
    t1_f = t1.astype(float)
    t2_f = t2.astype(float)
    # Interpolate time base
    inter_func = scipy.interpolate.interp1d(
        t2_f, y2, kind='linear', fill_value=[np.nan], bounds_error=False)
    y2_inter = inter_func(t1_f)

    if fill_outside:
        # Fill values outside interpolation bounds with nearest known value
        y2_inter[t1_f < np.min(t2_f)] = inter_func(np.min(t2_f))
        y2_inter[t1_f > np.max(t2_f)] = inter_func(np.max(t2_f))

    y2_matched = y2_inter
    return y2_matched

def match_timebase_sections(timebase, data_timebase, y,
                            section_diff):
    """
    Find values of pairs of points (`data_timebase`, `y`, both list-like) at times
    `timebase` (list-like), using linear interpolation.
    Data is split into sections where data is available, with sections
    separated by time `section_diff`.
    Returns interpolated data `y_ut`, mask `data_mask`, which is False where no
    input data were available, and indices of sections `sections`
    (index of first and last point in each section).
    """
    # Split data into sections where data is available
    gaps = np.where(np.diff(data_timebase) > section_diff)[0]
    sections = np.zeros((len(gaps)+1, 2), dtype=int)
    for i_section in range(len(sections)-1):
        sections[i_section, 1] = gaps[i_section]
        sections[i_section+1, 0] = gaps[i_section]+1
    sections[-1, 1] = len(data_timebase)-1

    # Interpolate sections on timebase
    y_ut = np.full(len(timebase), np.nan, dtype=y.dtype)
    data_mask = np.zeros(len(timebase), dtype=bool)
    for section in sections:
        y_ut_section = match_timebase(
            timebase,
            data_timebase[section[0]:section[1]+1],
            y[section[0]:section[1]+1],
            )
        section_mask = ~np.isnan(y_ut_section)
        y_ut_section_ = y_ut_section[section_mask].astype(y.dtype)
        y_ut[section_mask] = y_ut_section_
        data_mask[section_mask] = True

    return y_ut, data_mask, sections

def find_state_changed_inds(mask_on):
    """Find array of pair of indices between the value of boolean mask `mask_on` is constant."""
    mask_on = np.array(mask_on)
    state_changed_inds = np.where(np.diff(mask_on))[0]
    if mask_on[state_changed_inds][0] == True:
        state_changed_inds = np.hstack(([0], state_changed_inds))
    if mask_on[state_changed_inds][-1] == False:
        state_changed_inds = np.append(
            state_changed_inds, [len(mask_on)-1])
    state_changed_inds = np.reshape(
        state_changed_inds, (int(len(state_changed_inds)/2), 2))
    return state_changed_inds

def rollover_time_to_timedelta(rollover_time):
    """Convert rollover time `rollover_time` (str with format 'HH:MM') to `datetime.timedelta`."""
    return (
        datetime.datetime.strptime(rollover_time, '%H:%M')
        - datetime.datetime.strptime('0:0', '%H:%M'))

def meas_date_to_timestamp(meas_date, rollover_time):
    """
    Convert measurement date `meas_date`, which is of type `datetime.datetime`, `np.datetime64`,
    or `pd.Timestamp` with hour, minute, second set to zero, to timestamp of type `np.datetime64`,
    of first possible measurement time in that measurement date.
    (Same as first element of list-like returned by
    `meas_date_to_time_range(meas_date, rollover_time)`, which is called internally.)
    """
    return meas_date_to_time_range(meas_date, rollover_time)[0]

def meas_date_to_time_range(meas_date, rollover_time):
    """
    Convert measurement date `meas_date`, which is of type `datetime.datetime`, `np.datetime64`,
    or `pd.Timestamp`, with hour, minute, second set to zero, to list of timestamps of type
    `np.datetime64` of first and last possible measurement time in that measurement date.
    """
    timestamp_start = (
        pd.to_datetime(pd.to_datetime(meas_date).date())
        + rollover_time_to_timedelta(rollover_time))
    timestamp_end = timestamp_start + pd.Timedelta('1 days')
    if isinstance(meas_date, np.datetime64):
        timestamp_start = np.datetime64(timestamp_start)
        timestamp_end = np.datetime64(timestamp_end)
    return [timestamp_start, timestamp_end]

def get_meas_dates(timestamps, rollover_time):
    """
    Convert datetime timestamps `timestamps` (single value or list-like of `np.datetime64` or
    pandas Series of `pd.Timestamp`) to measurement date(s) (single value or list-like of
    `np.datetime64` or pandas Series of type ),
    containing only the date with hour, minute, second set to zero.
    Measurements that occurred before the `rollover_time` (str with format 'HH:MM')
    on a given day are given the measurement date corresponding to the previous day.
    """
    start_timedelta = rollover_time_to_timedelta(rollover_time)
    timestamps_ = pd.Series(pd.to_datetime(timestamps))
    dates = pd.to_datetime(
        (timestamps_ - start_timedelta).apply(lambda t: t.date()))
    if not isinstance(timestamps, pd.Series):
        dates = dates.values
    if not isinstance(timestamps, (list, np.ndarray, pd.Series)) \
            and len(dates) == 1:
        dates = dates[0]
    if isinstance(timestamps, list):
        dates = list(dates)
    return dates
