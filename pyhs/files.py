# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Various file functions, including finding and loading of table files such as
CSV, Excel.
"""

import os
import glob
import xlsxwriter
import numpy as np
import pandas as pd
from pathlib import Path

class PyhsFileError(Exception):
    """A file error occured."""

def find_table_files(search_dir, file_ext_to_read,
                     filebase_pattern='*'):
    """"
    Find table files (CSV, Excel, ...) in given directory `search_dir`
    with extensions in `file_ext_to_read` (list-like).
    """
    df_filepaths = [
        glob.glob(
            str(Path(search_dir, f'{filebase_pattern}.{file_ext}')))
        for file_ext in file_ext_to_read]
    df_filepaths = [elem for elem0 in df_filepaths for elem in elem0]
    if len(df_filepaths) > 0:
        df_filenames = [os.path.split(elem)[1] for elem in df_filepaths]
        # Remove Excel backups/temp files
        excel_temp = np.array([
            elem.startswith('~$')
            and os.path.splitext(elem)[1] in ['.xls', '.xlsx']
            for elem in df_filenames])
        df_filepaths = np.array(df_filepaths)[~excel_temp]
    return df_filepaths

def load_table_file(filepath, date_columns=None, index_col=None,
                    str_columns=None, **kwargs):
    """
    Read table file from path `filepath` (str) into pandas DataFrame,
    parsing dates in columns `date_columns` (list-like) and using columns `index_col`
    (str or list-like) as index/indices.
    """
    if date_columns is None:
        date_columns = []
    if str_columns is not None:
        dtype = {column: str for column in str_columns}
    else:
        dtype = None

    file_ext = os.path.splitext(filepath)[1]
    read_func = None
    if file_ext in ['.csv', '.dat']:
        read_func = pd.read_csv
    elif file_ext in ['.xls', '.xlsx']:
        read_func = pd.read_excel
        kwargs = {'engine': 'openpyxl', **kwargs}
        date_columns = []
    else:
        raise(PyhsFileError(
            f'load_table_file: Unsupported file format \'{file_ext}\' encountered'))

    # Load without parsing dates to find available columns
    df_columns = read_func(
        filepath,
        index_col=None,
        **kwargs,
        )
    date_columns = list(
        np.array(date_columns)[
            np.in1d(np.array(date_columns), df_columns.columns)])
    df = read_func(
        filepath,
        index_col=None,
        **{
            'parse_dates': date_columns,
            'dtype': dtype,
            **kwargs},
        )
    if index_col is not None:
        df = df.set_index(index_col)

    return df

def write_table_file(df, filepath, overwrite=False,
                    **kwargs):
    """Write table file from pandas DataFrame `df` to path `filepath` (str)."""
    if Path(filepath).exists() and not overwrite:
        raise(PyhsFileError(
            f'write_table_file: File \'{filepath}\' already exists'))

    file_ext = os.path.splitext(filepath)[1]
    write_func = None
    if file_ext in ['.csv', '.dat']:
        write_func = df.to_csv
    elif file_ext in ['.xlsx']:
        write_func = df.to_excel
    else:
        raise(PyhsFileError(
            f'write_table_file: Unsupported file format \'{file_ext}\' encountered'))

    # Write
    try:
        write_func(
            filepath, **kwargs
            )
    except xlsxwriter.exceptions.FileCreateError as e:
        raise(PyhsFileError(
            f'write_table_file: {e}'))

def join_paths_posix(*args):
    """Join paths in set args, returning a path with POSIX path separator "/"."""
    return Path(*args).as_posix()
