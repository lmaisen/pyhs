# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 10:46:18 2015

@author: Lothar Maisenbacher/MPQ

Based on (now lost) public source, license unclear.
"""

import scipy.optimize
import numpy as np

class fit2dgaussian:

    def gaussian(self, height, center_x, center_y, width_x, width_y, rotation, offset):
        """Returns a gaussian function with the given parameters"""
        width_x = float(width_x)
        width_y = float(width_y)

        rotation = np.deg2rad(rotation)
        center_x_tmp = center_x * np.cos(rotation) - center_y * np.sin(rotation)
        center_y_tmp = center_x * np.sin(rotation) + center_y * np.cos(rotation)

        center_x=center_x_tmp
        center_y=center_y_tmp

        def rotgauss(x, y):
            xp = x * np.cos(rotation) - y * np.sin(rotation)
            yp = x * np.sin(rotation) + y * np.cos(rotation)
            g = height*np.exp(
                -2*(((center_x-xp)/width_x)**2+
                  ((center_y-yp)/width_y)**2)) + offset
            return g
        return rotgauss

    def gaussianRotFixed(self, height, center_x, center_y, width_x, width_y, offset):
        return self.gaussian(height, center_x, center_y, width_x, width_y, 0, offset)

    def moments(self, data):
        """
        Calculate peak value and the first and second moments of `data` (2D array) to find estimates
        for the parameters of a 2D Gaussian distribution.
        Returns (all returned values are float) the peak value `height`,
        the first moment (center of mass of distribution) along the x- and y-direction, `x` and `y`,
        and the second moment (standard deviation of distribution) along the x- and y-direction,
        `width_x`, `width_y`.
        """
        X = np.arange(0, data.shape[0], dtype=float)
        Y = np.arange(0, data.shape[1], dtype=float)
        xint = data.sum(axis=0)
        yint = data.sum(axis=1)
        # Total of all pixels
        total = data.sum()
        # Find first moment along x-direction
        x = np.sum(X*yint)/total
        # Find first moment along y-direction
        y = np.sum(Y*xint)/total
        col = data[:, int(y)]
        width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/col.sum())
        row = data[int(x), :]
        width_y = np.sqrt(abs((np.arange(row.size)-x)**2*row).sum()/row.sum())
        height = data.max()
        return height, x, y, width_x, width_y, 0.0, 0.0

    def momentsRotFixed(self, data):
        """Returns (height, x, y, width_x, width_y)
        the gaussian parameters of a 2D distribution by calculating its
        moments """
        X = np.arange(0, data.shape[0])
        Y = np.arange(0, data.shape[1])
        xint = data.sum(axis=0)
        yint = data.sum(axis=1)
        x = np.sum(X*yint)/np.sum(yint)
        y = np.sum(Y*xint)/np.sum(yint)
        col = data[:, int(y)]
        width_x = np.sqrt(abs((np.arange(col.size)-y)**2*col).sum()/col.sum())/2.2
        #row = data[int(x), :]
        #width_y = np.sqrt(abs((np.arange(row.size)-x)**2*row).sum()/row.sum())/2.2
        height = data.max()

        def gauss1d(r,r0,A,w,c):
            return A*np.exp(-2*(r-r0)**2/w**2)+c

        maxrow=data[:,int(y)].argmax()
        maxcol=data[maxrow,:].argmax()
        xvals = np.arange(0,data.shape[1])
        popt, pcov = scipy.optimize.curve_fit(gauss1d, xvals, data[maxrow,:],(x,height,width_x,data[0][0]))

        return popt[1]/1.03, maxrow, maxcol,  1.05*popt[2], 1.05*popt[2], popt[3]

    def fitgaussian(self, data):
        """
        Returns (height, x, y, width_x, width_y)
        the gaussian parameters of a 2D distribution found by a fit
        """
        params = self.moments(data)
        errorfunction = lambda p: np.ravel(self.gaussian(*p)(*np.indices(data.shape)) - data)
        p, success = scipy.optimize.leastsq(errorfunction, params)
        return p

    def fitgaussianRotFixed(self, data):
        """Returns (height, x, y, width_x, width_y)
        the gaussian parameters of a 2D distribution found by a fit"""
        params = self.momentsRotFixed(data)
        errorfunction = lambda p: np.ravel(self.gaussianRotFixed(*p)(*np.indices(data.shape)) - data)
        p, success = scipy.optimize.leastsq(errorfunction, params)
        return p
