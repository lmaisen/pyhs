# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Contains generic helper functions that do not merit their own module so far.
"""

import string
import random
import hashlib
import logging
import sys

def get_uid(length=8):
    """
    Close-to-unique, but still human readable, alphanumerical identifier of length `length`
    (int, default 8).
    """
    return ''.join([random.choice(string.ascii_letters+string.digits) for _ in range(length)])

def get_hash_uuid(input_str):
    """'''
    Create 36 character long alphanumerical hash of string `string` in the format of an UUID.
    """
    hasher = hashlib.sha1(input_str.encode())
    hex_str = hasher.hexdigest()
    uuid = (
        hex_str[:8]+'-'+hex_str[8:12]+'-'+hex_str[12:16]
        +'-'+hex_str[16:20]+'-'+hex_str[20:32])
    return uuid

def set_up_command_line_logger():
    """Set up logging for logger 'mainlogger' with output to command line."""
    logger = logging.getLogger('mainlogger')
    logger.setLevel(logging.DEBUG)
    # Create console handler with a higher log level
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    # Create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    # Add the handlers to logger
    if len(logger.handlers) == 0:
        logger.addHandler(ch)
    return logger

def get_command_line_logger():
    """"Get handle for logger 'mainlogger', previously set-up with `set_up_command_line_logger`."""
    return logging.getLogger('mainlogger')

def toggle_timestamp(print_timestamp=True):
    """
    Toggle printing of timestamp in logger output, with printing active if `print_timestamp` is set
    to True (default).
    """
    logger = logging.getLogger('mainlogger')
    if len(logger.handlers) > 0:
        ch = logger.handlers[0]
        if print_timestamp:
            formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        else:
            formatter = logging.Formatter('%(levelname)s - %(message)s')
        ch.setFormatter(formatter)
