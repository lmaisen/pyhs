# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 10:47:21 2016

@author: Lothar Maisenbacher/MPQ
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import derivative

import pyhs.fitfunc

#%% Test analytical derivatives by comparing to numerical derivatives

plt.clf()
fig = plt.gcf()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)

x0 = 0
A = 2
Amp = 1
y0 = 0
eta = 3
GammaL = 1
GammaG = 3
dx = 10
x_fit = np.linspace(-20, 20, 1000)

#func = pyhs.fitfunc.Gaussian
#Df_func = pyhs.fitfunc.Df_Gaussian
#p = [x0, Amp, y0, GammaG]

func = pyhs.fitfunc.VoigtDoublet
Df_func = pyhs.fitfunc.Df_VoigtDoublet
p = [x0, Amp, y0, GammaL, GammaG, x0 + dx, Amp/2]

func = pyhs.fitfunc.VoigtDoubletEqualAmps
Df_func = pyhs.fitfunc.Df_VoigtDoubletEqualAmps
p = [x0, Amp, y0, GammaL, GammaG, x0 + dx]

#func = pyhs.fitfunc.Voigt
#Df_func = pyhs.fitfunc.Df_Voigt
#p = [x0, Amp, y0, GammaL, GammaG]

ax1.plot(
    x_fit,
    func(x_fit, *p)
    )

param_values = np.linspace(-5,5,1000)
#param_values = np.linspace(1,5,1000)
ps = np.zeros((len(param_values), len(p)))
ps[:, :] = p
i_param = 0
ps[:, i_param] = param_values

def Df_func_single(param):

    return func(0, *p[:i_param], param, *p[i_param+1:])

ax2.plot(param_values, [Df_func([0], *p)[0, i_param] for p in ps])

deriv = [derivative(Df_func_single, param, dx=1e-8) for param in param_values]
ax2.plot(
    param_values, deriv,
    linestyle='--'
    )
