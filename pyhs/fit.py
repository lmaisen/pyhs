# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Fit class, used for fitting various (nonlinear) models (fit functions) to data
by minimizing chi-squared.
"""

import warnings
import numpy as np
import pandas as pd
import scipy.optimize
import scipy.stats

# pyhs
import pyhs.fitfunc as fitfunc

class Fit:

    ## Define fit functions
    fit_funcs = {}
    fit_funcs_defaults = {
        # Name of fit function, will be used e.g. for labels
        'Name': '',
        # Fit parameter IDs in the order required by the actual fit function
        # These IDs will be used for saving the parameters in DataFrames
        'FitParamIDs': [],
        # Some parameters are defined to be positive, but they fit fun ction is
        # sign-independent - the parameter IDs listed here will be forced to be positive,
        # e.g. line widths
        'PositiveFitParamIDs': [],
        # Handle of fit function to be called by the curve fitting algorithm
        # Input parameters (x, *p) are an array of points x and a tuple of the fit parameters p
        'Func': None,
        # Jacobian matrix of derivatives of fit function
        'Df_func': None,
        # Uncertainty of fit result
        # Input parameters (x, p, pcov) are an array of points x
        # where the uncertainity is evaluated,
        # an array of optimal fit parameters p and a matrix of the covariances pcov
        'Sigma_func': None,
        # Covariances of fit result
        # List of (list of) pairs of parameter IDs for which the covariance
        # should be saved in DataFrame, e.g. [['CFR', 'Amp']]
        'CovParamIDs': [],
        # List of IDs to be used to save the covariances, e.g. ['CovCFRAmp']
        'CovIDs': [],
        # List of booleans, if True covariance is saved as correlations coefficient,
        # if False covariance itself is saved, e.g. [False]
        'CovSaveAsCorrCoeff': [],
        # Derived parameter IDs, i.e., parameters derived from combination of fit parameters
        # These IDs will be used for saving the parameters in DataFrames
        'DerivParamIDs': [],
        # Some parameters are defined to be positive, but they fit function is
        # sign-independent - the parameter IDs listed here will be forced to be positive,
        # e.g. line widths
        'PositiveDerivParamIDs': [],
        # Handle of functions to be called to calculate the derived parameters
        # Input parameters (p, pcov) are an array of the fit parameters p
        # and an array of the covariance matrix of the fit result
        'DerivParamFuncs': [],
        # Global fit, i.e., fit which has a global parameters that is
        # shared over multiple inidividual fits
        # ID of the fit function which is used for the individual fits;
        # the results of the individual fits are used as start values
        'GlobalInitialFitFuncID': '',
        # The ID of the parameter which is used as a global fit parameter
        'GlobalParamIDs': [],
        }
    fit_funcs['Gaussian'] = {
        'Name': 'Gaussian',
        'FitParamIDs': ['CFR', 'Amp', 'Offset', 'GammaG'],
        'PositiveFitParamIDs': ['GammaG'],
        'Func': fitfunc.Gaussian,
        'Df_func': fitfunc.Df_Gaussian,
        'DerivParamIDs': ['Gamma', 'RelOffset'],
        'PositiveDerivParamIDs': ['Gamma'],
        'DerivParamFuncs': [fitfunc.Gaussian_Gamma, fitfunc.Lineshape_RelOffset],
        }
    fit_funcs['GaussianBeam'] = {
        'Name': 'Gaussian beam',
        'FitParamIDs': ['x0', 'w', 'Amp', 'Offset'],
        'PositiveFitParamIDs': ['w'],
        'Func': fitfunc.GaussianBeam,
        'Df_func': fitfunc.Df_GaussianBeam,
        'DerivParamIDs': [],
        'PositiveDerivParamIDs': [],
        'DerivParamFuncs': [],
        }
    fit_funcs['Lorentzian'] = {
        'Name': 'Lorentzian',
        'FitParamIDs': ['CFR', 'Amp', 'Offset', 'GammaL'],
        'PositiveFitParamIDs': ['GammaL'],
        'Func': fitfunc.Lorentzian,
        'Df_func': fitfunc.Df_Lorentzian,
        'DerivParamIDs': ['Gamma', 'RelOffset'],
        'PositiveDerivParamIDs': ['Gamma'],
        'DerivParamFuncs': [fitfunc.Lorentzian_Gamma, fitfunc.Lineshape_RelOffset],
        'CovParamIDs': [
            ['CFR', 'Amp'], ['CFR', 'Offset'], ['CFR', 'GammaL']],
        'CovIDs': ['CorrCFRAmp', 'CorrCFROffset', 'CorrCFRGammaL'],
        'CovSaveAsCorrCoeff': [True] * 3,
        }
    fit_funcs['FanoLorentzian'] = {
        'Name': 'Fano-Lorentzian',
        'FitParamIDs': ['CFR', 'Amp', 'Offset', 'GammaL', 'Eta'],
        'PositiveFitParamIDs': ['GammaL'],
        'Func': fitfunc.FanoLorentzian,
        'Df_func': fitfunc.Df_FanoLorentzian,
        'DerivParamIDs': ['Gamma', 'RelOffset'],
        'PositiveDerivParamIDs': ['Gamma'],
        'DerivParamFuncs': [fitfunc.Lorentzian_Gamma, fitfunc.Lineshape_RelOffset],
        'CovParamIDs': [
            ['CFR', 'Amp'], ['CFR', 'Offset'], ['CFR', 'GammaL'], ['CFR', 'Eta']],
        'CovIDs': ['CorrCFRAmp', 'CorrCFROffset', 'CorrCFRGammaL', 'CorrCFREta'],
        'CovSaveAsCorrCoeff': [True] * 4,
        }
    fit_funcs['Voigt'] = {
        'Name': 'Voigt',
        'FitParamIDs': ['CFR', 'Amp', 'Offset', 'GammaL', 'GammaG'],
        'PositiveFitParamIDs': ['GammaL', 'GammaG'],
        'Func': fitfunc.Voigt,
        'Df_func': fitfunc.Df_Voigt,
        'DerivParamIDs': ['Gamma', 'RelOffset'],
        'PositiveDerivParamIDs': ['Gamma'],
        'DerivParamFuncs': [fitfunc.Voigt_Gamma, fitfunc.Lineshape_RelOffset],
        'CovParamIDs': [
            ['CFR', 'Amp'], ['CFR', 'Offset'], ['CFR', 'GammaL'], ['CFR', 'GammaG']],
        'CovIDs': ['CorrCFRAmp', 'CorrCFROffset', 'CorrCFRGammaL', 'CorrCFRGammaG'],
        'CovSaveAsCorrCoeff': [True] * 4,
        }
    fit_funcs['FanoVoigt'] = {
        'Name': 'Fano-Voigt',
        'FitParamIDs': ['CFR', 'Amp', 'Offset', 'GammaL', 'GammaG', 'Eta'],
        'PositiveFitParamIDs': ['GammaL', 'GammaG'],
        'Func': fitfunc.FanoVoigt,
        'Df_func': fitfunc.Df_FanoVoigt,
        'DerivParamIDs': ['Gamma', 'RelOffset'],
        'PositiveDerivParamIDs': ['Gamma'],
        'DerivParamFuncs': [fitfunc.Voigt_Gamma, fitfunc.Lineshape_RelOffset],
        'CovParamIDs': [
            ['CFR', 'Amp'], ['CFR', 'Offset'], ['CFR', 'GammaL'],
            ['CFR', 'GammaG'], ['CFR', 'Eta']],
        'CovIDs': [
            'CorrCFRAmp', 'CorrCFROffset', 'CorrCFRGammaL', 'CorrCFRGammaG', 'CorrCFREta'],
        'CovSaveAsCorrCoeff': [True] * 5,
        }
    fit_funcs['GlobalFanoVoigt'] = {
        'Name': 'Global Fano-Voigt',
        'FitParamIDs': ['CFR', 'Amp', 'Offset', 'GammaL', 'GammaG', 'Eta'],
        'PositiveFitParamIDs': ['GammaL', 'GammaG'],
        'Func': fitfunc.FanoVoigt,
        'Df_func': fitfunc.Df_FanoVoigt,
        'DerivParamIDs': ['Gamma', 'RelOffset'],
        'PositiveDerivParamIDs': ['Gamma'],
        'DerivParamFuncs': [fitfunc.Voigt_Gamma, fitfunc.Lineshape_RelOffset],
        'CovParamIDs': [
            ['CFR', 'Amp'], ['CFR', 'Offset'], ['CFR', 'GammaL'],
            ['CFR', 'GammaG'], ['CFR', 'Eta']],
        'CovIDs': [
            'CorrCFRAmp', 'CorrCFROffset', 'CorrCFRGammaL', 'CorrCFRGammaG', 'CorrCFREta'],
        'CovSaveAsCorrCoeff': [True] * 5,
        'GlobalInitialFitFuncID': 'FanoVoigt',
        'GlobalParamIDs': ['Eta'],
        }
    fit_funcs['VoigtDoublet'] = {
        'Name': 'Voigt doublet',
        'FitParamIDs': ['CFR1', 'Amp1', 'Offset', 'GammaL', 'GammaG', 'CFR2', 'Amp2'],
        'PositiveFitParamIDs': ['GammaL', 'GammaG'],
        'Func': fitfunc.VoigtDoublet,
        'Df_func': fitfunc.Df_VoigtDoublet,
        'DerivParamIDs': [
            'CFR', 'DeltaNu', 'AmpRatio', 'Gamma', 'RelOffset', 'Amp', 'AlphaDeltaNu'],
        'PositiveDerivParamIDs': ['DeltaNu', 'Gamma'],
        'DerivParamFuncs': [fitfunc.VoigtDoublet_CFR, fitfunc.VoigtDoublet_DeltaNu,
                            fitfunc.VoigtDoublet_AmpRatio,
                            fitfunc.Voigt_Gamma, fitfunc.VoigtDoublet_RelOffset,
                            fitfunc.VoigtDoublet_AmpAvg,
                            lambda p, pcov: (np.nan, np.nan)],
        }
    fit_funcs['VoigtDoubletEqualAmps'] = {
        'Name': 'Voigt doublet (equ. amp)',
        'FitParamIDs': ['CFR1', 'Amp', 'Offset', 'GammaL', 'GammaG', 'CFR2'],
        'PositiveFitParamIDs': ['GammaL', 'GammaG'],
        'Func': fitfunc.VoigtDoubletEqualAmps,
        'Df_func': fitfunc.Df_VoigtDoubletEqualAmps,
        'DerivParamIDs': ['CFR', 'DeltaNu', 'Gamma', 'RelOffset', 'AlphaDeltaNu'],
        'PositiveDerivParamIDs': ['DeltaNu', 'Gamma'],
        'DerivParamFuncs': [fitfunc.VoigtDoublet_UnweightedCFR, fitfunc.VoigtDoublet_DeltaNu,
                            fitfunc.Voigt_Gamma, fitfunc.Lineshape_RelOffset,
                            lambda p, pcov: (np.nan, np.nan)],
        }
    fit_funcs['GenericLineshape'] = {
        'Name': 'Generic lineshape',
        'FitParamIDs': [
            'CFR', 'Amp', 'Offset', 'GammaL', 'GammaG', 'Eta',
            'CFR1', 'Amp1', 'CFR2', 'Amp2'],
        'PositiveFitParamIDs': ['GammaL', 'GammaG'],
        'Func': None,
        'Df_func': None,
        'DerivParamIDs': [
            'DeltaNu', 'AmpRatio', 'Gamma', 'RelOffset', 'AlphaDeltaNu'],
        'PositiveDerivParamIDs': ['DeltaNu', 'Gamma'],
        'DerivParamFuncs': [None] * 5,
        }
    fit_funcs['Linear'] = {
        'Name': 'Linear fit',
        'FitParamIDs': ['Slope', 'Offset'],
        'Func': fitfunc.Linear,
        'Df_func': fitfunc.Df_Linear,
        'Sigma_func': fitfunc.Sigma_Linear,
        'CovParamIDs': [['Slope', 'Offset']],
        'CovIDs': ['Cov01'],
        'CovSaveAsCorrCoeff': [False],
        }
    fit_funcs['Parabola'] = {
        'Name': 'Parabola',
        'FitParamIDs': ['QuadrCoeff', 'Center', 'Offset'],
        'Func': fitfunc.Parabola,
        'Df_func': fitfunc.Df_Parabola,
        }
    fit_funcs['Avg'] = {
        'Name': 'Average',
        'FitParamIDs': ['Avg'],
        'Func': lambda x, y0: np.repeat(y0, len(x)),
        'Df_func': lambda x, y0: np.repeat(1, len(x)).reshape((len(x), 1)),
        'Sigma_func':  lambda x, p, pcov: np.repeat(pcov[0, 0], len(x)),
        }
    fit_funcs['GenMaxwellBoltzmann'] = {
        'Name': 'Generalized Maxwell-Boltzmann',
        'FitParamIDs': ['n', 'Amp', 'v0'],
        'Func': fitfunc.GenMaxwellBoltzmann,
        }
    fit_funcs['GenMaxwellBoltzmannCutoff'] = {
        'Name': 'Generalized Maxwell-Boltzmann with cutoff speed',
        'FitParamIDs': ['n', 'Amp', 'v0', 'vcut'],
        'Func': fitfunc.GenMaxwellBoltzmannCutoff,
        }
    fit_funcs['ErrorFctBeamRadius'] = {
        'Name': 'Complimentary error function',
        'FitParamIDs': ['x0', 'A', 'w', 'y0'],
        'Func': fitfunc.ErrorFctBeamRadius,
        'Df_func': fitfunc.Df_ErrorFctBeamRadius,
        }
    fit_funcs['RedChiSq'] = {
        'Name': 'Reduced χ^2 distribution',
        'FitParamIDs': ['RCSAmp', 'RCSDOF'],
        'Func': lambda x, amp, dof: amp*scipy.stats.chi2.pdf(x*dof, dof),
        }
    fit_funcs['RedChiSqShifted'] = {
        'Name': 'Shifted reduced χ^2 distribution',
        'FitParamIDs': ['RCSAmp', 'RCSShift', 'RCSDOF'],
        'Func': lambda x, amp, shift, dof: (
            amp*scipy.stats.chi2.pdf((x-shift)*dof, dof)),
        }
    fit_funcs['Sine'] = {
        'Name': 'Sine',
        'FitParamIDs': ['SineAmp', 'SinePeriod', 'SineShift', 'SineOffset'],
        'Func': fitfunc.Sine,
        }
    fit_funcs['SineDeg'] = {
        'Name': 'Sine (deg.)',
        'FitParamIDs': ['SineAmp', 'SinePeriodDeg', 'SineShiftDeg', 'SineOffset'],
        'Func': fitfunc.SineDeg,
        }
    fit_funcs['QIJ12'] = {
        'Name': 'QI J=1/2 (sinusoidal)',
        'FitParamIDs': ['SineAmp', 'SineShiftDeg', 'SineOffset'],
        'Func': lambda x, A, x0, y0: fitfunc.SineDeg(x, A, 1/180, x0, y0),
        'DerivParamIDs': ['QIAmp', 'QIMagicAngle'],
        'PositiveDerivParamIDs': ['DeltaNu', 'Gamma'],
        'DerivParamFuncs': [
            lambda p, pcov: (np.abs(p[0]), np.sqrt(pcov[0, 0])),
            fitfunc.QIJ12_MagicAngle,
            ],
        }
    fit_funcs['CapCharging'] = {
        'Name': 'Exponential charging of capacitor',
        'FitParamIDs': ['RiseTime', 'MaxValue', 'RelativeChange', 'TimeOffset'],
        'Func': lambda t, tau, a, b, t0: a*(1-b*np.exp(-(t-t0)/tau)),
        }
    DEFAULT_LINE_FIT_FUNCS = ['Lorentzian', 'FanoLorentzian', 'Voigt', 'FanoVoigt']

    ## Define meta data for fit functions
    # Fit parameter names
    fit_param_names = {
        'ChiSq': 'chi-square',
        'RedChiSq': 'Red. chi-square',
        'CFR': 'Res. freq.',
        'Amp': 'Amplitude',
        'Offset': 'Offset',
        'RelOffset': 'Rel. offset',
        'GammaL': 'Lorentzian FWHM',
        'GammaG': 'Gaussian FWHM',
        'Gamma': 'FWHM line width',
        'Eta': 'Asymmetry parameter',
        'CFR1': 'Res. freq. 1',
        'CFR2': 'Res. freq. 2',
        'Amp1': 'Amplitude 1',
        'Amp2': 'Amplitude 2',
        'DeltaNu': 'Freq. splitting',
        'AmpRatio': 'Amp. ratio 1/2',
        'AlphaDeltaNu': 'Alpha angle from freq. splitting',
        'CorrCFRAmp': 'Correlation of res. freq. and amplitude',
        'CorrCFROffset': 'Correlation of res. freq. and offset',
        'CorrCFRGammaL': 'Correlation of res. freq. and Lorentzian FWHM',
        'CorrCFRGammaG': 'Correlation of res. freq. and Gaussian FWHM',
        'CorrCFREta': 'Correlation of res. freq. and asymmetry parameter',
        'x0': 'Position',
        'w': '1/e^2 radius',
        }
    # Fit parameter symbols
    fit_param_symbols = {
        'ChiSq': r'$\chi^{2}_{}$',
        'RedChiSq': r'$\chi^{2}_{\mathrm{red}}$',
        'CFR': r'$\nu^{}_0$',
        'Amp': r'$A$',
        'Offset': r'$y^{}_0$',
        'RelOffset': r'$y^{}_0/A$',
        'GammaL': r'$\Gamma^{}_{\mathrm{L}}$',
        'GammaG': r'$\Gamma^{}_{\mathrm{G}}$',
        'Gamma': r'$\Gamma^{}_{\mathrm{F}}$',
        'Eta': r'$\eta$',
        'CFR1': r'$\nu^{}_1$',
        'CFR2': r'$\nu^{}_2$',
        'Amp1': r'$A^{}_1$',
        'Amp2': r'$A^{}_2$',
        'DeltaNu': r'$\Delta\nu^{}_0$',
        'AmpRatio': r'$A^{}_1/A^{}_2$',
        'AlphaDeltaNu': r'$\alpha_{\Delta\nu_0}$',
        'CorrCFRAmp': r'$r_{\nu_0,A}$',
        'CorrCFROffset': r'$r_{\nu_0,y_0}$',
        'CorrCFRGammaL': r'$r_{\nu_0,\Gamma_L}$',
        'CorrCFRGammaG': r'$r_{\nu_0,\Gamma_G}$',
        'CorrCFREta': r'$r_{\nu_0,\eta}$',
        'x0': r'$x^{}_0$',
        'w': r'$w^{}_0$',
        }
    # Default plot label for fit parameters
    fit_param_plot_label_default = '$FitParamName $FitParamSymbol'
    # If not the default combination of fit parameter name and symbol should
    # be used as plot label, define custom label here
    fit_param_plot_labels_custom = {
        'ChiSq': '$FitParamSymbol',
        'RedChiSq': '$FitParamSymbol',
        }
    # Fit parameter units
    fit_param_units = {
        'ChiSq': '',
        'RedChiSq': '',
        'CFR': 'Hz',
        'Amp': 'Cts',
        'Offset': 'Cts',
        'RelOffset': '',
        'GammaL': 'Hz',
        'GammaG': 'Hz',
        'Gamma': 'Hz',
        'Eta': '',
        'CFR1': 'Hz',
        'CFR2': 'Hz',
        'Amp1': 'Cts',
        'Amp2': 'Cts',
        'DeltaNu': 'Hz',
        'AmpRatio': '',
        'AlphaDeltaNu': 'rad',
        'CorrCFRAmp': '',
        'CorrCFROffset': '',
        'CorrCFRGammaL': '',
        'CorrCFRGammaG': '',
        'CorrCFREta': '',
        'x0': 'm',
        'w': 'm',
        }
    # Names of methods to further process scan fits (fit derivatives)
    fit_deriv_names = {
        'DlysAvg': 'delay avg.',
        'DlysLin': 'delay extrapol.',
        }
    # Average/extrapolation of fit parameter: Names
    fit_deriv_param_names = {
        'ChiSq': '$FitParamName $FitDerivID chi-squared',
        'RedChiSq': '$FitParamName chi-squared',
        'Avg': 'avg.',
        'Slope': 'slope',
        'Offset': 'extrapol.',
        'Cov': 'covariance',
        }
    # Average/extrapolation of fit parameter: Units
    fit_deriv_param_units = {
        'ChiSq': '',
        'RedChiSq': '',
        'Avg': '$FitParamUnit',
        'Slope': '$FitParamUnit/(m/s)',
        'Offset': '$FitParamUnit',
        'Cov': '$FitParamUnit^2/(m/s)',
        }
    # Average/extrapolation of fit parameter: Symbols
    fit_deriv_param_symbols = {
        'ChiSq': r'$\chi^2$',
        'RedChiSq': r'$\chi^2_\mathrm{red}$',
        'Avg': '',
        'Slope': '',
        'Offset': '',
        'Cov': '',
        }
    # Default plot label for average/extrapolation of fit parameters
    fit_deriv_param_plot_label_default = '$FitParamName $FitDerivParamName'
    # If not the default combination of fit parameter name and symbol should
    # be used as plot label, define custom label here
    fit_deriv_param_plot_labels_custom = {
        'ChiSq': '$FitParamName $FitDerivName $FitDerivParamSymbol',
        'RedChiSq': '$FitParamName $FitDerivName $FitDerivParamSymbol',
        }

    ## Default fit algorithm options
    # 'Tol': Value for `ftol`, `xtol`, `gtol` for `least_squares`
    # 'MaxFEv': Maximum iterations for `least_squares`
    # 'EpsFcn': Value of `epsfcn` for `least_squares`
    # Note that here `least_squares` is called through `scipy.optimize.curve_fit`
    # interface.
    DEFAULT_FIT_OPT = {'Tol': 1e-14, 'MaxFEv': 1000, 'EpsFcn': 1e-14}

    # Default floating point error handling, as set through `np.seterr` in method `do_fit`.
    # If set to 'warn', a warning is printed; if set to 'raise', a `FloatingPointError` is raised.
    # I have observed a different behavior on Windows and Linux systems with regards to
    # the handling of invalid operations ('invalid') during curve fitting:
    # on Windows, no invalid error was raised and the fit (in this case a Voigt fit) converged;
    # on Linux, an invalid operation error was raised and the fit stopped.
    # Setting `'warn': 'ignore'` allowed the fit to progress to the same result as on Windows,
    # with the earlier error not leading to erroneous results.
    # Thus, by default, `'invald': 'warn'` is used.
    DEFAULT_FP_ERR_HANDLING = {
        'divide': 'raise', 'over': 'raise', 'under': 'raise', 'invalid': 'warn'}

    def __init__(self, out_func=None, **kwds):
        """Initialize."""
        self.fp_err_handling = self.DEFAULT_FP_ERR_HANDLING.copy()
        if 'fp_err_handling' in kwds:
            self.fp_err_handling = {**self.fp_err_handling, **kwds['fp_err_handling']}
            del kwds['fp_err_handling']
        self.fit_opt = {**self.DEFAULT_FIT_OPT, **kwds}
        self.fit_funcs = {
            id_: {**self.fit_funcs_defaults, **params}
            for (id_, params) in self.fit_funcs.items()}
        self.out_func = out_func
        # Deprecated references
        self.fitFuncs = self.fit_funcs

    def do_fit(self, fit_func, x, y, y_sigma=None, out_func=None,
               warning_as_error=True, debug=False, debug_out_func=None,
               remove_nan_points=False, **kwargs):
        """
        Perform nonlinear curve fit for a given fit function.

        Parameters
        ----------
        fit_func : dict
            Dictionary containing fit function and metadata.
        x : ndarray, shape (n,)
            n data points along x-axis.
        y : ndarray, shape (n,)
            n data points along y-axis.
        y_sigma : ndarray, shape (n,), optional
            Uncertainties of data points along y-axis.
            If not given, they will be assumed to be one.
        out_func : func, optional
            Function used to output messages.
            The default is None, in which case `print` will be used.
        warning_as_error : bool, optional
            If set to True, NumPy and scipy.optimize warnings will
            be raised as errors. Default is True.
        debug : bool, optional
            If set to True, additional debug messages will be printed.
            Default is False.
        debug_out_func : func, optional
            Function used to output debug messages.
            The default is None, in which case the function specified by `out_func` is used.
        remove_nan_points : bool, optional
            If set to True, any points for which the corresponding value of `x`, `y`, or `y_sigma`
            is NaN are remove. Otherwise, the data is not altered.
            The default is False.

        Returns
        -------
        fit_result : dict
            Dictionary containing the fit result.
        error_msg : str
            Error message, empty if no errors occured.
        """
        np_err_handling = np.geterr()
        if warning_as_error:
            # Define handling of floating point errors.
            # If they are set to `raise`, they can be caught as exception below.
            # These error settings will be restored to their previous value afterwards.
            np.seterr(**self.fp_err_handling)
            # Raise `scipy.optimize.OptimizeWarning` as exception.
            # Unfortunately, there is no easy way to remove this filter afterwards and it here
            # remains set.
            warnings.simplefilter('error', scipy.optimize.OptimizeWarning)

        if y_sigma is None:
            y_sigma = np.ones(len(x))
        if out_func is None:
            out_func = print if self.out_func is None else self.out_func
        debug_out_func = out_func if debug_out_func is None else debug_out_func
        func = fit_func['Func']
        Df_func = fit_func['Df_func']
        num_params = len(fit_func['FitParamIDs'])
        ier = -1
        fit_error = False
        error_msg = ''
        num_deriv_params = len(fit_func['DerivParamIDs'])
        deriv_popt = np.full(num_deriv_params, np.nan)
        deriv_perr = np.full(num_deriv_params, np.nan)
        pstart = kwargs.get('pstart', np.ones(num_params))
        if remove_nan_points:
            # Remove NaN points
            x = np.atleast_1d(x)
            y = np.atleast_1d(y)
            y_sigma = np.atleast_1d(y_sigma)
            mask_nan = (~np.isnan(x)) & (~np.isnan(y)) & (~np.isnan(y_sigma))
            if np.sum(~mask_nan) > 0 and debug:
                debug_out_func(
                    f'Removed {np.sum(~mask_nan):d} NaN point(s) from data')
            x = x[mask_nan]
            y = y[mask_nan]
            y_sigma = y_sigma[mask_nan]
        if debug:
            debug_out_func(
                'Fitting function \'{}\' with start parameters {}'
                .format(
                    fit_func['Name'],
                    ', '.join(['{} = {:.3e}'.format(id_, p)
                               for p, id_ in zip(pstart, fit_func['FitParamIDs'])])))

        try:
            if Df_func is not None:
                popt_A, pcov_A, infodict, mesg, ier = scipy.optimize.curve_fit(
                    func, x, y, pstart, sigma=y_sigma, absolute_sigma=True,
                    method='lm', ftol=self.fit_opt['Tol'], xtol=self.fit_opt['Tol'],
                    gtol=self.fit_opt['Tol'],
                    jac=Df_func, maxfev=self.fit_opt['MaxFEv'], full_output=True)
            else:
                popt_A, pcov_A, infodict, mesg, ier = scipy.optimize.curve_fit(
                    func, x, y, pstart, sigma=y_sigma, absolute_sigma=True,
                    method='lm', ftol=self.fit_opt['Tol'], xtol=self.fit_opt['Tol'],
                    gtol=self.fit_opt['Tol'],
                    maxfev=self.fit_opt['MaxFEv'], full_output=True,
                    epsfcn=self.fit_opt['EpsFcn'])
            # Check returned pcov
            if np.any(np.diag(pcov_A) < 0):
                raise scipy.optimize.OptimizeWarning(
                    'Covariance matrix has negative diagonal elements')
            # Cast parameters listed in PositiveFitParamIDs as positive
            pos_iids = [
                i for i, elem in enumerate(fit_func['FitParamIDs'])
                if elem in fit_func['PositiveFitParamIDs']]
            popt_A[pos_iids] = np.abs(popt_A[pos_iids])
            perr_A = np.sqrt(np.diag(pcov_A))
            chiSq_A = np.sum((y-func(x, *popt_A))**2/y_sigma**2)
            redChiSq_A = (
                chiSq_A/(len(x)-len(popt_A))
                if (len(x)-len(popt_A)) > 0 else np.nan)
        except (
                ValueError, TypeError, FloatingPointError, RuntimeError,
                RuntimeWarning, scipy.optimize.OptimizeWarning) \
                as err:
            error_msg = (
                f'Fit \'{fit_func["Name"]}\' did not converge: {err}')
            fit_error = True
            if out_func is not None:
                out_func(error_msg)
        if not fit_error:
            fit_error, deriv_popt, deriv_perr = self.calc_deriv_params(
                fit_func, popt_A, pcov_A, out_func=out_func)
        if fit_error:
            popt = np.full(num_params, np.nan)
            perr = np.full(num_params, np.nan)
            pcov = np.full((num_params, num_params), np.nan)
            fit_result = {
                'Popt': popt, 'Pcov': pcov, 'Perr': perr,
                'ChiSq': np.nan, 'RedChiSq': np.nan,
                'FitError': fit_error, 'FitIEr': ier,
                'DerivPopt': deriv_popt, 'DerivPerr': deriv_perr,
                }
        else:
            fit_result = {
                'Popt': popt_A, 'Pcov': pcov_A, 'Perr': perr_A,
                'ChiSq': chiSq_A, 'RedChiSq': redChiSq_A,
                'FitError': fit_error, 'FitIEr': ier,
                'DerivPopt': deriv_popt, 'DerivPerr': deriv_perr,
                }
        if debug:
            debug_out_func(
                'Fit result (red. chi-squared: {:.2f}): {}'
                .format(
                    fit_result['RedChiSq'],
                    ', '.join(['{} = {:.3e}'.format(id_, p)
                               for p, id_ in zip(fit_result['Popt'], fit_func['FitParamIDs'])])))

        if warning_as_error:
            # Restore previous floating point error handling
            np.seterr(**np_err_handling)

        return fit_result, error_msg

    @np.deprecate(new_name='do_fit')
    def doFit(self, *args, **kwargs):

        return self.do_fit(*args, **kwargs)

    def calc_deriv_params(self, fit_func, popt, pcov, out_func=None):
        """
        Calculated derived parameters for fit function `fit_func`, optimal fit parameters `popt`,
        and optimal covariances `pcov`. Derived parameters are parameters that can be calculated
        from the fit parameters, and likewise their uncertainties can be derived from the
        covariances of the fit parameters.
        An example of derived parameter is the total linewidth of a Voigt line shape, which can
        be derived from the fitted parameters of the Gaussian and Lorentzian linewidths.
        The optional parameter `out_func` takes a function used to output messages
        (rhe default is None, in which case `print` will be used).
        """
        if out_func is None:
            out_func = print if self.out_func is None else self.out_func
        fit_error = False
        num_deriv_params = len(fit_func['DerivParamIDs'])
        deriv_popt = np.full(num_deriv_params, np.nan)
        deriv_perr = np.full(num_deriv_params, np.nan)
        for i, (deriv_param_id, deriv_param_func) in \
        enumerate(zip(fit_func['DerivParamIDs'], fit_func['DerivParamFuncs'])):
            try:
                deriv_popt[i], deriv_perr[i] = deriv_param_func(popt, pcov)
            except (TypeError, FloatingPointError, RuntimeError, RuntimeWarning) as err:
                error_msg = (
                    'Derived parameter \'{}\' for fit \'{}\' could not be calculated: {}'
                    .format(deriv_param_id, fit_func['Name'], err))
                if out_func is not None:
                    out_func(error_msg)
                fit_error = True
        pos_iids = [i for i, elem in enumerate(fit_func['DerivParamIDs'])
                    if elem in fit_func['PositiveDerivParamIDs']]
        deriv_popt[pos_iids] = np.abs(deriv_popt[pos_iids])

        return fit_error, deriv_popt, deriv_perr

    @np.deprecate(new_name='calc_deriv_params')
    def calcDerivParams(self, *args, **kwargs):

        return self.calc_deriv_params(*args, **kwargs)

    def fit_line(self, x, y, y_sigma,
                 debug=False, startGammaL=np.nan, startGammaG=np.nan,
                 out_func=print, fit_func_ids=None, pstarts=None,
                 force_error_for_flat_y_data=False, debug_out_func=None,
                 **kwargs):
        """
        Fit line, such as a frequency scan over an atomic resonance, with various line shape
        functions.

        Parameters
        ----------
        x : ndarray, shape (n,)
            n data points along x-axis.
        y : ndarray, shape (n,)
            n data points along y-axis.
        y_sigma : ndarray, shape (n,), optional
            Uncertainties of data points along y-axis.
            If not given, they will be assumed to be one.
        debug : bool, optional
            If set to True, additional debug messages will be printed.
            Default is False.
        startGammaL : float, optional
            Start value for the Lorentzian FWHM linewidth (in Hz).
            Default is `np.nan`, which will be replaced with 1e7 Hz.
        startGammaG : float, optional
            Start value for the Gaussian FWHM linewidth (in Hz).
            Default is `np.nan`, which will be replaced with 1e7 Hz.
        out_func : func, optional
            Function used to output messages.
            If not given, `print` will be used.
        fit_func_ids : array-like, optional
            List or array of IDs of fit functions which should be fit to the data.
            If set to None or an empty list, `self.DEFAULT_LINE_FIT_FUNCS` is used.
        pstarts : dict, optional
            Dictionary containing values of start parameters for specific fit functions.
            The values for each fit functions with ID `fit_func_id` can be stored in a dictionary
            `pstarts[fit_func_id]`, with keys of the dictionary corresponding to the IDs
            of the parameters and the values to start values.
            If set to None, the default start values defined or calculated within this method are
            used.
        force_error_for_flat_y_data : bool, optional
            If set to True, and the input y-values `y` are all identical, i.e., y-data is flat,
            no fit to the data will be attempted, but a result corresponding to a failed fit will
            be returned instead. Default is False.
        debug_out_func : func, optional
            Function used to output debug messages.
            The default is None, in which case the function specified by `out_func` is used.

        Returns
        -------
        line_fits : dict
            A dictionary containing the fit result, itself a dict, for each of the fit functions,
            with the fit function ID serving as key.
        """
        # Set default start parameters for Lorentzian and Gaussian FWHM linewidths `startGammaL`
        # and `startGammaG`
        startGammaL = 1e7 if np.isnan(startGammaL) else startGammaL
        startGammaG = 1e7 if np.isnan(startGammaG) else startGammaG

        # Check if input y-values `y` are all identical, i.e., y-data is flat.
        # If `force_error_for_flat_y_data` is to True, a fit to such data will not be attempted,
        # but a result corresponding to a failed fit will be returned instead.
        y_data_flat = np.all(np.isclose(y, y[0]))

        # Estimate start parameters from data
        if np.sum(y) == 0:
            weights = np.ones(len(x))
        else:
            weights = y
        x0_start = np.average(x, weights=weights)
        # Pre-factor of 4 gives approx. FWHM of a Gaussian, while pre-factor of 2.6 gives
        # approx. FWHM of a Lorentzian. Using the mean, 3.3.
        gamma_est = 3.3*np.sqrt(np.log(2))*np.sqrt(np.average((x-x0_start)**2, weights=weights**2))

        line_fits = {}
        sorted_fit_func_ids = np.array([
            'Lorentzian', 'FanoLorentzian', 'Voigt', 'FanoVoigt',
            'LorentzianLinOffset', 'FanoLorentzianLinOffset', 'VoigtLinOffset', 'FanoVoigtLinOffset'
            ])
        if fit_func_ids is None or len(fit_func_ids) == 0:
            available_fit_func_ids = np.array(self.DEFAULT_LINE_FIT_FUNCS)
        else:
            available_fit_func_ids = np.array(list(fit_func_ids))
        if pstarts is None:
            pstarts = {}
        mask = np.in1d(sorted_fit_func_ids, available_fit_func_ids)
        maskNotInSorted = ~np.in1d(available_fit_func_ids, sorted_fit_func_ids)
        for fit_func_id in np.hstack((
                sorted_fit_func_ids[mask], available_fit_func_ids[maskNotInSorted])):

            fit_func = self.fit_funcs[fit_func_id]

            pstart0 = [x0_start, np.max(y), 0, gamma_est]
            if fit_func_id == 'Lorentzian':
                pstart = pstart0
            elif fit_func_id == 'FanoLorentzian':
                pstart = [
                    line_fits['Lorentzian']['Popt'][0],
                    line_fits['Lorentzian']['Popt'][1],
                    line_fits['Lorentzian']['Popt'][2],
                    line_fits['Lorentzian']['Popt'][3],
                    0]
            elif fit_func_id == 'Voigt':
                if 'Lorentzian' in line_fits:
                    guessGammaG = fitfunc.Voigt_GammaG(
                        line_fits['Lorentzian']['Popt'][3], startGammaL)
                    if np.isnan(guessGammaG):
                        guessGammaG = startGammaG
                    pstart = [
                        line_fits['Lorentzian']['Popt'][0],
                        line_fits['Lorentzian']['Popt'][1],
                        line_fits['Lorentzian']['Popt'][2],
                        startGammaL,
                        guessGammaG]
                else:
                    pstart = [*pstart0[0:3], startGammaL, startGammaG]
            elif fit_func_id == 'FanoVoigt':
                if 'FanoLorentzian' in line_fits:
                    pstart = [
                        line_fits['FanoLorentzian']['Popt'][0],
                        line_fits['Voigt']['Popt'][1],
                        line_fits['Voigt']['Popt'][2],
                        line_fits['Voigt']['Popt'][3],
                        line_fits['Voigt']['Popt'][4],
                        line_fits['FanoLorentzian']['Popt'][4]]
                else:
                    pstart = [
                        line_fits['Voigt']['Popt'][0],
                        line_fits['Voigt']['Popt'][1],
                        line_fits['Voigt']['Popt'][2],
                        line_fits['Voigt']['Popt'][3],
                        line_fits['Voigt']['Popt'][4],
                        0]
            elif fit_func_id == 'LorentzianLinOffset':
                pstart = [
                    line_fits['Lorentzian']['Popt'][0],
                    line_fits['Lorentzian']['Popt'][1],
                    line_fits['Lorentzian']['Popt'][2],
                    line_fits['Lorentzian']['Popt'][3],
                    0]
            elif fit_func_id == 'FanoLorentzianLinOffset':
                pstart = [
                    line_fits['FanoLorentzian']['Popt'][0],
                    line_fits['FanoLorentzian']['Popt'][1],
                    line_fits['FanoLorentzian']['Popt'][2],
                    line_fits['FanoLorentzian']['Popt'][3],
                    line_fits['FanoLorentzian']['Popt'][4],
                    0]
            elif fit_func_id == 'VoigtLinOffset':
                pstart = [
                    line_fits['Voigt']['Popt'][0],
                    line_fits['Voigt']['Popt'][1],
                    line_fits['Voigt']['Popt'][2],
                    line_fits['Voigt']['Popt'][3],
                    line_fits['Voigt']['Popt'][4],
                    0]
            elif fit_func_id == 'FanoVoigtLinOffset':
                pstart = [
                    line_fits['FanoVoigt']['Popt'][0],
                    line_fits['FanoVoigt']['Popt'][1],
                    line_fits['FanoVoigt']['Popt'][2],
                    line_fits['FanoVoigt']['Popt'][3],
                    line_fits['FanoVoigt']['Popt'][4],
                    line_fits['FanoVoigt']['Popt'][5],
                    0]
            elif fit_func_id == 'VoigtDoublet':
                if 'Voigt' in line_fits:
                    pstart = np.hstack((line_fits['Voigt']['Popt'], [0, 0]))
                else:
                    pstart = [*pstart0[0:3], startGammaL, startGammaG,0, 0]
                pstart[1] = pstart[1]/2
                pstart[5:7] = pstart[0:2]
                if fit_func_id in pstarts and 'DeltaNu' in pstarts[fit_func_id]:
                    pstart[0] -= pstarts[fit_func_id]['DeltaNu']/2
                    pstart[5] += pstarts[fit_func_id]['DeltaNu']/2
            elif fit_func_id == 'VoigtDoubletEqualAmps':
                pstart = np.hstack((line_fits['Voigt']['Popt'], [0]))
                pstart[5] = pstart[0]
                if fit_func_id in pstarts and 'DeltaNu' in pstarts[fit_func_id]:
                    pstart[0] -= pstarts[fit_func_id]['DeltaNu']/2
                    pstart[5] += pstarts[fit_func_id]['DeltaNu']/2
            else:
                pstart = np.ones(len(fit_func['FitParamIDs']))
            # Replace estimated start parameters with explicitly given
            # start parameters passed to method
            if fit_func_id in pstarts:
                pstart_ = pstart.copy()
                pstart = np.array([
                    float(pstarts[fit_func_id][fit_param_id])
                    if fit_param_id in pstarts[fit_func_id]
                    else pstart_[fit_param_iid]
                    for fit_param_iid, fit_param_id in enumerate(fit_func['FitParamIDs'])
                    ])

            if force_error_for_flat_y_data and y_data_flat:
                line_fits[fit_func_id] = self.get_dummy_fit_result(fit_func)
            else:
                line_fits[fit_func_id], error_msg = self.do_fit(
                    fit_func, x, y, y_sigma, pstart=pstart,
                    out_func=out_func, debug=debug, debug_out_func=debug_out_func,
                    **kwargs)

        return line_fits

    @np.deprecate(new_name='fit_line')
    def fitLine(self, *args, **kwargs):

        return self.fit_line(*args, **kwargs)

    def sr_to_fit_result(self, sr_fit, fit_func, column_prefix=None, column_prefix_params=None):
        """
        Converts a fit result stored in a pandas Series `sr_fit`, where every key contains a single
        value (i.e., no array-like structures) into a `fit_result` dictionary, as returned by the
        method `do_fit`.
        The underlying fit function (dict) must be supplied as `fit_func`.
        `column_prefix` and `column_prefix_params` define a prefix (str) for the keys in the Series
        `sr_fit` for keys containing fit metadata and fit parameters, respectively. If set to None
        (default), no prefix is used.
        """
        column_prefix_ = column_prefix+'_' if column_prefix is not None else ''
        column_prefix_params_ = column_prefix_params+'_' if column_prefix_params is not None else ''
        num_params = len(fit_func['FitParamIDs'])
        num_deriv_params = len(fit_func['DerivParamIDs'])
        pcov = np.full((num_params, num_params), np.nan)
        fit_result = {
            'Popt': np.zeros(num_params),
            'Perr': np.zeros(num_params),
            'ChiSq': sr_fit[column_prefix_+'ChiSq'],
            'RedChiSq': sr_fit[column_prefix_+'RedChiSq'],
            'FitError': sr_fit[column_prefix_+'Error'],
            'FitIEr': -1,
            'DerivPopt': np.zeros(num_deriv_params),
            'DerivPerr': np.zeros(num_deriv_params),
            }
        popt_columns = [column_prefix_params_+param_id+'_'+'Value'
                        for param_id in fit_func['FitParamIDs']]
        fit_result['Popt'] = sr_fit[popt_columns].values
        perr_columns = [column_prefix_params_+param_id+'_'+'Sigma'
                        for param_id in fit_func['FitParamIDs']]
        perr = sr_fit[perr_columns].values
        fit_result['Perr'] = perr
        deriv_popt_columns = [column_prefix_params_+param_id+'_'+'Value'
                              for param_id in fit_func['DerivParamIDs']]
        fit_result['DerivPopt'] = sr_fit[deriv_popt_columns].values
        deriv_perr_columns = [column_prefix_params_+param_id+'_'+'Sigma'
                              for param_id in fit_func['DerivParamIDs']]
        fit_result['DerivPerr'] = sr_fit[deriv_perr_columns].values
        for cov_param_ids, cov_id, save_as_corr_coeff in zip(
                fit_func['CovParamIDs'], fit_func['CovIDs'],
                fit_func['CovSaveAsCorrCoeff']):
            cov_param_iids = [
                cov_param_iid for cov_param_iid, cov_param_id
                in enumerate(fit_func['FitParamIDs'])
                if cov_param_id in cov_param_ids]
            column = column_prefix_params_+cov_id+'_'+'Value'
            pcov[cov_param_iids[0], cov_param_iids[1]] = (
                sr_fit[column] if column in sr_fit.index else np.nan)
            if save_as_corr_coeff:
                pcov[cov_param_iids[0], cov_param_iids[1]] *= (
                    sr_fit[column_prefix_params_+cov_param_ids[0]+'_'+'Sigma']
                    * sr_fit[column_prefix_params_+cov_param_ids[1]+'_'+'Sigma'])
            pcov[cov_param_iids[1], cov_param_iids[0]] = (
                pcov[cov_param_iids[0], cov_param_iids[1]])
        np.fill_diagonal(pcov, perr**2)
        fit_result['Pcov'] = pcov

        return fit_result

    def fit_result_to_dict(self, fit_result, fit_func, column_prefix=None):
        """
        Converts a `fit_result` dictionary, as returned by the method `do_fit`,
        into a fit result stored in a dictionary `result_dict`, where every key contains a single
        value (i.e., no array-like structures).
        If the resulting dict is converted into a pandas Series
        (as done by method `fit_result_to_series`, which calls this method), this method
        is the inverse of `sr_to_fit_result`.
        The underlying fit function (dict) must be supplied as `fit_func`.
        `column_prefix` defines a prefix (str) for the keys in the dict `result_dict`.
        If set to None (default), no prefix is used.
        """
        column_prefix_ = column_prefix+'_' if column_prefix is not None else ''
        result_dict = {}
        result_dict[column_prefix_+'ChiSq'] = fit_result['ChiSq']
        result_dict[column_prefix_+'RedChiSq'] = fit_result['RedChiSq']
        result_dict[column_prefix_+'Error'] = fit_result['FitError']
        fit_params_value = {
            column_prefix_ + fit_param_id + '_Value': fit_result['Popt'][fit_param_iid]
            for fit_param_iid, fit_param_id in enumerate(fit_func['FitParamIDs'])
            }
        fit_params_sigma = {
            column_prefix_ + fit_param_id + '_Sigma': fit_result['Perr'][fit_param_iid]
            for fit_param_iid, fit_param_id in enumerate(fit_func['FitParamIDs'])
            }
        deriv_params_value = {
            column_prefix_ + deriv_param_id + '_Value': fit_result['DerivPopt'][deriv_param_iid]
            for deriv_param_iid, deriv_param_id in enumerate(fit_func['DerivParamIDs'])
            }
        deriv_params_sigma = {
            column_prefix_ + deriv_param_id + '_Sigma': fit_result['DerivPerr'][deriv_param_iid]
            for deriv_param_iid, deriv_param_id in enumerate(fit_func['DerivParamIDs'])
            }
        cov_value = {}
        for cov_param_ids, cov_id, save_as_corr_coeff in zip(
                fit_func['CovParamIDs'], fit_func['CovIDs'], fit_func['CovSaveAsCorrCoeff']):
            cov_param_iids = [cov_param_iid for cov_param_iid, cov_param_id
                              in enumerate(fit_func['FitParamIDs'])
                              if cov_param_id in cov_param_ids]
            if save_as_corr_coeff:
                cov_value[column_prefix_+cov_id+'_Value'] = (
                    fit_result['Pcov'][cov_param_iids[0], cov_param_iids[1]]
                    / fit_result['Perr'][cov_param_iids[0]]
                    / fit_result['Perr'][cov_param_iids[1]])
            else:
                cov_value[column_prefix_+cov_id+'_Value'] = (
                    fit_result['Pcov'][cov_param_iids[0], cov_param_iids[1]])

        result_dict = {
            **result_dict, **fit_params_value, **fit_params_sigma,
            **deriv_params_value, **deriv_params_sigma, **cov_value}
        return result_dict

    def fit_result_to_series(self, *args, **kwargs):
        """
        Calls method `fit_result_to_dict` and converts the returned dict into a
        pandas Series `sr_fit`.
        """
        sr_fit = pd.Series(self.fit_result_to_dict(*args, **kwargs))
        return sr_fit

    def get_dummy_fit_result(self, fit_func):
        """
        Get the equivalent of a dummy `fit_result` dictionary with
        all optimal parameters set to `np.nan` and `FitError` set to True,
        as returned by method `do_fit` when a fit fails.
        The underlying fit function (dict) must be supplied as `fit_func`.
        """
        num_params = len(fit_func['FitParamIDs'])
        num_deriv_params = len(fit_func['DerivParamIDs'])
        ier = -1
        fit_error = True
        popt = np.full(num_params, np.nan)
        perr = np.full(num_params, np.nan)
        pcov = np.full((num_params, num_params), np.nan)
        deriv_popt = np.full(num_deriv_params, np.nan)
        deriv_perr = np.full(num_deriv_params, np.nan)
        fit_result = {
            'Popt': popt, 'Pcov': pcov, 'Perr': perr,
            'ChiSq': np.nan, 'RedChiSq': np.nan,
            'FitError': fit_error, 'FitIEr': ier,
            'DerivPopt': deriv_popt, 'DerivPerr': deriv_perr,
            }
        return fit_result

    def get_dummy_fit_result_dict(self, fit_func, column_prefix=None):
        """
        Get the equivalent of a dummy `fit_result` dictionary with
        all optimal parameters set to `np.nan` and `Error` set to True,
        as returned by method `do_fit` when a fit fails,
        converted into a dict `result_dict` with a single value per key.
        The underlying fit function (dict) must be supplied as `fit_func`.
        `column_prefix` defines a prefix (str) for the keys in the dict `result_dict`.
        If set to None (default), no prefix is used.
        """
        result_dict = self.fit_result_to_dict(
            self.get_dummy_fit_result(fit_func), fit_func, column_prefix)
        return result_dict

    def get_fixed_fit_func(self, fit_func, fixed_params):
        """
        Return fit function defined in dict `fit_func` with parameters in dict
        `fixed_params` held constant and removed from fit.
        So far only implemented for functions without analytical derivatives.
        """
        fit_func_fixed = fit_func.copy()
        fit_param_ids = fit_func['FitParamIDs']
        fixed_param_ids = list(fixed_params.keys())
        ind_free = np.where(~np.in1d(
            fit_param_ids, fixed_param_ids))[0]
        ind_fixed = np.where(np.in1d(
            fit_param_ids, fixed_param_ids))[0]
        p_combined = np.zeros(len(fit_param_ids))
        p_combined[ind_fixed] = list(fixed_params.values())

        def func(x, *p, p_combined=p_combined, ind_free=ind_free):

            p_combined[ind_free] = p
            return fit_func['Func'](x, *p_combined)

        fit_func_fixed['Func'] = func
        fit_func_fixed['FitParamIDs'] = np.array(fit_func['FitParamIDs'])[ind_free]

        return fit_func_fixed
