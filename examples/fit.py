# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 12:00:52 2019

@author: Lothar Maisenbacher/MPQ
"""

import numpy as np
import matplotlib.pyplot as plt

# pyhs
import pyhs.fit

# Init fit class
myFit = pyhs.fit.Fit()

# Open figure
plt.figure(
    'pyhs.fit',
    constrained_layout=True)
plt.clf()
ax1 = plt.gca()

# File to fit
filepath = '5EHxl4KOA0P30'

# Fit functions to use (see myFit.fit_funcs.keys())
active_fit_func_ids = [
    'Lorentzian',
    'Voigt',
    'FanoVoigt',
#    'VoigtDoublet',
    ]

# Fit parameters
fit_params = {
    # Initial guess of Lorentzian FWHM
    'startGammaL': 3.9e6,
    # Initial guess of Gaussian FWHM
    'startGammaG': 5e6,
    'debug': True,
    }

# Load file
data = np.loadtxt(filepath)
# Select line
x = data[:, 0]
y = data[:, 10].transpose()
y_sigma = np.ones(len(x))

line_fits = myFit.fit_line(
    x, y, y_sigma,
    fit_func_ids=active_fit_func_ids,
    **fit_params,
    )

# Plot data
h1_data = ax1.plot(
    x,
    y,
    label='Data',
    linestyle='', marker='o', markersize=3,
    )

# Print and plot fit results
for fit_func_id, fit_result in line_fits.items():
    fit_func = myFit.fit_funcs[fit_func_id]
    sr_fit = myFit.fit_result_to_series(
        fit_result, fit_func)
    print(
        '{}: CFR = {:.8f} Hz, Gamma = {:.3e} Hz, Amp = {:.5} photons'
        .format(
            fit_func_id,
            sr_fit['CFR_Value'],
            sr_fit.get('Gamma_Value', np.nan),
            sr_fit.get('Amp_Value', np.nan),
            ))
    # Plot fit
    x_fit = np.linspace(np.min(x), np.max(x), 1000)
    h1_fit = ax1.plot(
        x_fit,
        fit_func['Func'](
            x_fit, *fit_result['Popt']),
        label=fit_func_id,
        )

ax1.legend()
ax1.set_xlabel('Frequency')
ax1.set_ylabel('Signal')
