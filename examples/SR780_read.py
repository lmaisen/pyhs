# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Reads data from SRS SR780 FFT network signal analyzer.
"""

import numpy as np
import matplotlib.pyplot as plt
import time
from pathlib import Path

import pyhs.devices.SR780

## Settings
# File output directory
dir_output = ''
# Filename prefix
filename = 'SR780'
# Device address
address = 'GPIB0::10::INSTR'

## Read device
# Init SR780 class
SR780 = pyhs.devices.SR780.SR780()

# Open connection
SR780.openConn(address)

# Active display
display = 0

# Get measurement parameters and FFT frequencies
params, fft_freq = SR780.getParams(display)

# Get measurement data
start_time = time.perf_counter()
data = SR780.instH.query(f'DSPY ? {display:d}')
fft_data = np.fromstring(data, sep=',')
print(f'Reading data took {time.perf_counter()-start_time:.2f} s')

params['Time'] = time.strftime("%Y-%m-%d %H-%M-%S")
params['Comment'] = ''
# params['Columns'] =  'Freq (Hz), {} ({})'.format(SR780.view_label[params['View']], params['Unit'])

# Save data to file
header_dict = [key + ': ' + str(value) for key, value in params.items()]
header = '\n'.join(header_dict)
np.savetxt(
    Path(dir_output, filename + '.dat'),
    np.transpose(np.vstack((fft_freq, fft_data))),
    delimiter=',', header=header)

# Plot FFT data
fig, axs = plt.subplots(num='SR780', constrained_layout=True, clear=True)
ax = axs
label = params['Comment']
if params['Avg']:
    label += ', {:d} avgs.'.format(params['NAvg'])
if SR780.units_log[params['Unit']]:
    ax.plot(fft_freq, fft_data, label=label)
else:
    ax.semilogy(fft_freq, fft_data, label=label)
ax.legend(loc='upper right')
ax.set_xlabel('Frequency (Hz)')
ax.set_ylabel('{} ({})'.format(SR780.view_label[params['View']], SR780.units_label[params['Unit']]))
#ax1.set_title(params['Comment'])
# fig.savefig(Path(dir_output, filename + '.pdf'))
