# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

"""

import matplotlib.pyplot as plt

import pyhs.devices.DSOX

# Open figure
plt.figure(figsize =(8, 6))
plt.clf()

params = {'text.usetex' : False,
          'font.size' : 10,
          'font.family' : 'Times New Roman',
          'mathtext.fontset' : 'stix',
          }
plt.rcParams.update(params)
ax1 = plt.gca()

# Init scope class (Keysight DSOX)
DSOX = pyhs.devices.DSOX.DSOX()

# Read from file
dataFolder = ''
pltsFolder = dataFolder
fileprefix = 'DSOX_test'
timebase, data, params = DSOX.readFile(dataFolder + fileprefix + '.npz')

# Plot data
ax1 = plt.gca()
ax1.cla()
for i, chIID in enumerate(DSOX.chIIDs):
    label = '{}'.format(DSOX.chNames[chIID])
    ax1.plot(timebase, data[i], label=label)
ax1.legend(loc='best')

ax1.set_xlabel(DSOX.xName + (' ({})'.format(DSOX.xUnit) if DSOX.xUnit!='' else ''))
ax1.set_ylabel(DSOX.yName + (' ({})'.format(DSOX.yUnit) if DSOX.yUnit!='' else ''))
ax1.set_title(params['Time'])
plt.tight_layout()

#plt.savefig(pltsFolder + params['Time'] + '.pdf')
#plt.savefig(pltsFolder + params['Time'] + '.png', dpi=300)

#DSOX.writeFile(dataFolder + fileprefix + '.dat')
