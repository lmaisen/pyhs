# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Example for connecting to and continously reading data from LakeShore F71 teslameter
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from collections import OrderedDict

import pyhs.devices.F71

# Using this routine from https://stackoverflow.com/a/45734500 does not bring the figure into focus on each update
def mypause(interval):
    backend = plt.rcParams['backend']
    if backend in mpl.rcsetup.interactive_bk:
        figManager = mpl._pylab_helpers.Gcf.get_active()
        if figManager is not None:
            canvas = figManager.canvas
            if canvas.figure.stale:
                canvas.draw()
            canvas.start_event_loop(interval)
            return

# Init teslameter class (LakeShore F71)
F71 = pyhs.devices.F71.F71()

# Read from teslameter
address = 'TCPIP0::192.168.0.121::8888::SOCKET'
F71.openConn(address)
# Set number of integer averages of 10 ms samples
navg = 1
F71.setAveraging(navg)

## Teslameter channels
teslameter_channels = OrderedDict()
for id_, f71_id, name_ in zip(
        ['X', 'Y', 'Z', 'Magn'],
        ['X', 'Y', 'Z', 'Magn'],
        ['x', 'y', 'z', 'magn.']
        ):
    teslameter_channels['MagF71' + id_] = {
        'F71ID': f71_id,
        'Name': 'F71 ' + name_,
        'Scaling': 1,
        'Converter': lambda v: v,
        'ConverterNoOffset': lambda v: v,
        }

plt.ion()

params = {'text.usetex' : False,
          'font.size' : 16,
          'font.family' : 'Times New Roman',
          }
plt.rcParams.update(params)

plt.clf()
ax1 = plt.gca()

num_samples = 100

time = np.arange(-num_samples, 0)
data = np.zeros((num_samples, 4))
data[:] = np.nan

hs = {}
for i, (id_, channel) in enumerate(teslameter_channels.items()):
    hs[id_] = ax1.plot(
        time,
        data[:, i],
        marker='o',
        label=channel['Name']
        )[0]
ax1.legend(loc='upper left')
ax1.set_ylabel('Field (mG)')
ax1.set_xlabel('Time (s)')
plt.tight_layout()

while True:

    try:

        data = np.roll(data, -1, axis=0)

        # Read field (in mG)
        reading = F71.readField()
        print(reading)

        for i, (id_, channel) in enumerate(teslameter_channels.items()):
            data[-1, i] = reading[channel['F71ID']]
            hs[id_].set_ydata(data[:, i])

        ax1.relim()
        ax1.autoscale_view(False,True,True)
        plt.draw()

        mypause(F71.getAvgTime())

    except KeyboardInterrupt:
        break

F71.closeConn()
