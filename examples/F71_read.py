# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Example for connecting to and reading data from LakeShore F71 teslameter
"""

import pyhs.devices.F71

# Init teslameter class (LakeShore F71)
F71 = pyhs.devices.F71.F71()

# Read from teslameter
address = 'TCPIP0::192.168.0.121::8888::SOCKET'
F71.openConn(address)
# Set number of integer averages of 10 ms samples
navg = 1
F71.setAveraging(navg)

# Read field (in mG)
data = F71.readField()
print(data)

F71.closeConn()
