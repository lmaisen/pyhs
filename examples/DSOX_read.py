# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

"""

import matplotlib.pyplot as plt

import pyhs.devices.DSOX

plt.figure(figsize =(8, 6))
plt.clf()

params = {'text.usetex' : False,
          'font.size' : 10,
          'font.family' : 'Times New Roman',
          'mathtext.fontset' : 'stix',
          }
plt.rcParams.update(params)
ax1 = plt.gca()

dataFolder = ''
pltsFolder = dataFolder

# Init scope class (Keysight DSOX)
DSOX = pyhs.devices.DSOX.DSOX()

# Read from scope
address = 'TCPIP0::192.168.0.109::inst0::INSTR'
DSOX.openConn(address)
# Configure scope
DSOX.confDataTransfer()

DSOX.setMaxNPoints()
chIIDs = [1,2,3,4] # Channels to read
chNames = {elem: 'Ch{:d}'.format(elem) for elem in chIIDs}
DSOX.setChannelNames(chNames)
timebase, data, params = DSOX.readChannels(chIIDs)
DSOX.setComment('243 nm enhancement cavity finesse measurement')
fileprefix = params['Time']
fileprefix = 'DSOX_test'
data_filename = DSOX.writeFile(dataFolder + fileprefix + '.npz')
DSOX.closeConn()

# Plot data
ax1 = plt.gca()
ax1.cla()
plotMultiplier = [1,1,1,1]
for i, ch in enumerate(chIIDs):
    label = 'Ch {}: {}'.format(str(ch),chNames[ch])
    ax1.plot(timebase, plotMultiplier[i]*data[i], label=label)
ax1.legend(loc='best')

ax1.set_xlabel('Time (s)')
ax1.set_ylabel('Voltage (V)')
ax1.set_title(params['Time'])
plt.tight_layout()

#plt.savefig(pltsFolder + params['Time'] + '.pdf')
#plt.savefig(pltsFolder + params['Time'] + '.png', dpi=300)
