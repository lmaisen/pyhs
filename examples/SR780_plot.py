# -*- coding: utf-8 -*-
"""
@author: Lothar Maisenbacher/MPQ

Reads data from SRS SR780 FFT network signal analyzer
"""

import matplotlib.pyplot as plt
from pathlib import Path

import pyhs.devices.SR780

# Open figure
plt.figure('SR780', figsize=(8, 6), constrained_layout=True)
plt.clf()

params = {'text.usetex' : False,
          'font.size' : 10,
          'font.family' : 'Times New Roman',
          'mathtext.fontset' : 'stix',
          }
plt.rcParams.update(params)
ax1 = plt.gca()

# Init SR780 class
SR780 = pyhs.devices.SR780.SR780()

## Read from file
dataFolder = ''
pltsFolder = dataFolder
fileprefix = 'SR780'
FFTfreq, data, params = SR780.readFile(
    Path(dataFolder, fileprefix + '.dat'))
FFTdata = data[0]

# Plot FFT data
ax1 = plt.gca()
label = params['Comment']
if params['Avg']:
    label += (', ' if label != '' else '') + f'{params["NAvg"]:d} avgs.'
if SR780.units_log[params['Unit']]:
    ax1.semilogx(FFTfreq, FFTdata, label=label)
    # ax1.plot(FFTfreq, FFTdata, label=label)
else:
    ax1.loglog(FFTfreq, FFTdata, label=label)
    # ax1.semilogy(FFTfreq, FFTdata, label=label)
ax1.legend(loc='upper right',fontsize=12)
ax1.set_xlabel('Frequency (Hz)')
ax1.set_ylabel(
    f'{SR780.view_label[params["View"]]} ({SR780.units_label[params["Unit"]]})')
#ax1.set_title(params['Comment'])
#plt.savefig(outFolder + outFile + '.pdf')
#plt.savefig(outFolder + outFile + '.png', dpi=300)

#%% Read and plot multiple FFTs

#inFiles = []
#inFolder = 'SR780_data//'
##Acceleration detector
##inFiles.append('2018-03-22 16-04-44') # Chamber rim
#inFiles.append('2018-03-22 16-07-29') # Chamber floor
#inFiles.append('2018-03-22 16-09-14') # Cavity spacer
#inFiles.append('2018-03-22 16-10-57') # Inner cavity spacer
#
#outFolder = inFolder
#outFile = '2018-03-22 Chamber vibrations'
#
#plt.clf()
#ax1 = plt.gca()
#
#for inFile in inFiles:
#    params, FFTfreq, FFTdata = SR780.readFFTfile(inFolder + inFile + '.dat')
#    label = params['Comment']
#    ax1.plot(FFTfreq, FFTdata, label=label)
#
#ax1.legend(loc='upper right',fontsize=12, ncol=2)
#ax1.set_xlabel('Frequency (Hz)')
#ax1.set_ylabel('{} ({})'.format(SR780.view_label[params['View']], SR780.units_label[params['Unit']]))
#plt.tight_layout()
#plt.savefig(outFolder + outFile + '.pdf')
#plt.savefig(outFolder + outFile + '.png', dpi=300)
